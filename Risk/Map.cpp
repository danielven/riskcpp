//
//  Map.cpp
//
#include "stdafx.h"
#include "Map.h"
#include "Country.h"
#include "Player.h"
#include "Continent.h"
#include "ContinentAlreadyExists.h"
# include <iostream>
# include <string>
# include <vector>
#include <algorithm>

Map::Map(){};



//Methods adds a country to the map, make sure that country A is adjacent to country B.
void Map::addCountry(Country* countryA, Country* countryB){

	if((countryExist(countryA)) && (countryExist(countryB)))
	{
		cout << "Country is already in the Map!" << endl;
	}

	else if(countryExist(countryA))
	{
		_countries.push_back(countryB);
		if (!(continentExist(countryB->getContinent())))
		{
			_continents.push_back(countryB->getContinent());

		}
		countryA->makeAdjacent(countryB);
	}
	else if(countryExist(countryB))
	{
		_countries.push_back(countryA);
		if (!(continentExist(countryA->getContinent())))
		{
			_continents.push_back(countryA->getContinent());

		}
		countryB->makeAdjacent(countryA);
	}


	Notify();

}

void Map::addCountry(Country* country){

	_countries.push_back(country);

	/*
	if (!continentExist(country->getContinent()))
	_continents.push_back(country->getContinent());
	*/
	Notify();
}

//Method that checks whether a country exists.
bool Map::countryExist(Country* country){
	bool exist = false;
	for (vector<Country*>::iterator country_it = _countries.begin() ; country_it != _countries.end(); ++country_it){
		if( country->equal(*country_it)){
			exist = true;
			break;
		}
	}

	return exist;
}


void Map::addContinent(Continent* continent){
	if (continentExist(continent)) {
		throw ContinentAlreadyExists();
	} else {
		_continents.push_back(continent);
	}

	Notify();
}



Country* Map::getCountry(string country){

	for (vector<Country*>::iterator cont_it = _countries.begin() ; cont_it != _countries.end(); ++cont_it){
		if( country==(*cont_it)->getName()){
			return (*cont_it);
			break;
		}
	}

	cout << "Country was not found";


	return nullptr;

}



Continent* Map::getContinent(string continentName){

	transform(continentName.begin(), continentName.end(), continentName.begin(), ::tolower);

	for (vector<Continent*>::iterator cont_it = _continents.begin() ; cont_it != _continents.end(); ++cont_it){
		{
			string name= (*cont_it)->getName();
			transform(name.begin(), name.end(), name.begin(), ::tolower);
			if( continentName==name){
				return (*cont_it);
				break;
			}
		}
	}

	cout << "Continent was not found\n";


	return nullptr;
}

//Method that checks whether a continent exists.
bool Map::continentExist(Continent* continent){
	bool exist = false;
	for (vector<Continent*>::iterator cont_it = _continents.begin() ; cont_it != _continents.end(); ++cont_it){
		if( (*continent).equal(*cont_it)){
			exist = true;
			break;
		}
	}

	return exist;
}

//Method that makes the generic map from the textfile imported from  http://www.windowsgames.co.uk.
void Map::genericMapCreator(){
	createMap("GenericMap.txt");
	Notify();
}

void Map::createMap(string fileName){

	ifstream file(fileName);

	char line[256];


	if (file.is_open())
	{
		file.getline(line, 256);


		if (string(line)=="Continents"){


			file.getline(line,256,',');



			while (string(line)!="Countries")
			{
				string contName=line;

				file.getline(line,256);

				string num=line;

				Continent* continent= new Continent(contName, stoi(num));

				try {
					this->addContinent(continent);
				} catch (ContinentAlreadyExists e) {
					cout << e.what() << endl;
				}
				file.getline(line,256, ',');

			}

		}

		file.getline(line,256);

		while (file.getline(line,256,',')){

			string name=line;
			file.getline(line,256,',');
			int xCoord = atoi(line);
			file.getline(line, 256, ',');
			int yCoord = atoi(line);

			file.getline(line,256,',');

			Continent* continent=this->getContinent(line);

			Country* country= new Country(name, continent, xCoord, yCoord);

			file.getline(line,256);

			if(!this->countryExist(country))
				this->addCountry(country);

		}
		file.close();
	}


	else cout << "Unable to open file"; 

	ifstream file2(fileName);

	char word[256];

	if (file2.is_open()){

		file2.getline(word, 256);


		if (string(word)=="Continents"){

			file2.getline(word,256, ',');

			while (string(word)!="Countries")
			{
				file2.getline(word, 256);

				file2.getline(word,256, ',');
			}

		}

		file2.getline(word, 256);

		while (file2.getline(word,256,',')){

			Country* country=this->getCountry(word);

			file2.getline(word,256,',');
			file2.getline(word,256,',');
			file2.getline(word,256,',');

			file2.getline(word,256);

			string adjacentCountries=word;

			string delimiter = ",";

			size_t pos = 0;
			string token;

			while ((pos = adjacentCountries.find(delimiter)) != string::npos) {
				token = adjacentCountries.substr(0, pos);
				Country* country2= this->getCountry(token);
				country->makeAdjacent(country2);

				adjacentCountries.erase(0, pos + delimiter.length());
			}
			Country* country2= this->getCountry(adjacentCountries);
			country->makeAdjacent(country2);

		}

	}

	else cout << "Unable to open file"; 

	Notify();
}

void Map::createMapWithArmies(string fileName){

	ifstream file(fileName);

	char line[256];


	if (file.is_open())
	{
		file.getline(line, 256);


		if (string(line)=="Continents"){


			file.getline(line,256,',');



			while (string(line)!="Countries")
			{
				string contName=line;

				file.getline(line,256);

				string num=line;

				Continent* continent= new Continent(contName, stoi(num));

				try {
					this->addContinent(continent);
				} catch (ContinentAlreadyExists e) {
					cout << e.what() << endl;
				}
				file.getline(line,256, ',');

			}

		}

		file.getline(line,256);

		while (file.getline(line,256,',')){

			if (string(line)=="Players")
				break;

			string name=line;
			file.getline(line,256,',');
			int xCoord = atoi(line);
			file.getline(line, 256, ',');
			int yCoord = atoi(line);

			file.getline(line, 256, ',');
			Continent* continent=this->getContinent(line);		//add exception

			Country* country = new Country(name, continent, xCoord, yCoord);

			file.getline(line,256);

			if(!this->countryExist(country))
			{

				this->addCountry(country);

			}


		}
		file.close();
	}


	else cout << "Unable to open file"; 

	ifstream file2(fileName);

	char word[256];

	if (file2.is_open()){

		file2.getline(word, 256);


		if (string(word)=="Continents"){

			file2.getline(word,256, ',');

			while (string(word)!="Countries")
			{
				file2.getline(word, 256);

				file2.getline(word,256, ',');
			}

		}

		file2.getline(word, 256);

		while (file2.getline(word,256,',')){

			if (string(word)=="Players")
				break;



			Country* country=this->getCountry(word);

			file2.getline(word,256,',');
			file2.getline(word,256,',');
			file2.getline(word,256,',');


			file2.getline(word,256);

			string adjacentCountries=word;

			string delimiter = ",";

			size_t pos = 0;
			string token;

			while ((pos = adjacentCountries.find(delimiter)) != string::npos) {
				token = adjacentCountries.substr(0, pos);
				Country* country2= this->getCountry(token);
				country->makeAdjacent(country2);

				adjacentCountries.erase(0, pos + delimiter.length());
			}

			token = adjacentCountries.substr(0, pos);
			country->addArmy(stoi(token));
		}

	}

	else
	{
		cout << "Unable to open file"; 
		system("PAUSE");
		exit(EXIT_FAILURE);
	}

	Notify();
}


vector<Country*> Map::getCountryVector(){

	return _countries;

}


//print the countries in the map along with, their continent, the number of armies and countries they are adjacent to. Move to view
void Map::printMap(){

	cout << "\nThere are " << _countries.size() << " countries and " << _continents.size() << " continents in the Map:\n" << endl;

	for (vector<Country*>::iterator country_it = _countries.begin() ; country_it != _countries.end(); ++country_it){

		cout << " * "<< (*country_it)->getName() << " is located in " << (*country_it)->getContinent()->getName()  << " ,is adjacent to " << (*country_it)->getNumberOfPaths() << " countries, is owned by "<< (*country_it)->getPlayer()->getName() << " and has "  << (*country_it)->getArmies() << " armies." << endl;

	}

}


void Map::removeCountry(Country* country){

	for( vector<Country*>::iterator it = _countries.begin(); it != _countries.end(); ++it )
	{
		if( *it == country )
		{
			vector<Country*> _paths= country->getPaths();

			for (vector<Country*>::iterator it2 = _paths.begin(); it2 != _paths.end(); ++it2 )
			{
				vector<Country*> _paths2 = (*it2)->getPaths();

				for (vector<Country*>::iterator it3 = _paths2.begin(); it3 != _paths2.end(); ++it3)
					if (*it3==country)
					{
						_paths2.erase(std::remove(_paths2.begin(), _paths2.end(), country), _paths2.end()); 
						break;
					}

			}

			_countries.erase( it );
			break;
		}
	}
	Notify();
}

void Map::removeContinent(Continent* continent){

	for( vector<Continent*>::iterator it = _continents.begin(); it != _continents.end(); ++it )
	{
		if( *it == continent )
		{
			vector<Country*> countryVector= continent->getCountries();

			for (vector<Country*>::iterator it2 = countryVector.begin(); it2 != countryVector.end(); ++it2 )
			{				
				removeCountry(*it2);
			}

			_continents.erase( it );
			break;
		}
	}
	Notify();
}

void Map::addCountryToContinent(Country* country, Continent* continent){

	if (std::find(_continents.begin(), _continents.end(),continent)!=_continents.end())
	{
		continent->addCountry(country);
	}
	Notify();
}

