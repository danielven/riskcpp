#include "stdafx.h"
#include "Battle.h"
#include "Country.h"
#include "ComputerPlayer.h"
#include "Dice.h"
#include "iostream"
#include <typeinfo>


using namespace std;

const int MAX_NUM_ATTACKING_ARMIES=3; //According to risk rules, each attack round can be done with at most 3 armies.
const int MIN_NUM_DEFENDING_ARMIE=2; //According to risk rules, defender will defend with at most 2 armies.

Battle::Battle(Country* const &attacker, Country* const &defender)
{

	// verify that attacker is adjacent to defender and that attacker is a different player than defender
	if (!attacker->isAdjacentTo(defender) || attacker->getPlayer() == defender->getPlayer()) {
		throw std::invalid_argument("defender");
	}
	Battle::attacker = attacker;
	Battle::defender = defender;
	Battle::status = false;
	Battle::view = new BattleView();

}


bool Battle::doBattle(bool allIn) {

	if (attacker->getArmies()<=1)
	{
		view->notEnoughArmies(attacker, defender);
		return false;
	}

	//Variable will control whether attacking player wants to continue attacking.
	bool attacking=true;

	while(attacking==true){

		view->displayAttackerInfo(attacker);
		view->displayDefenderInfo(defender);

		int numOfAttackingArmies;

		if (typeid(*attacker->getPlayer())!= typeid(ComputerPlayer)) {
			// for human player
			numOfAttackingArmies = view->getNumberOfAttackingArmies(attacker, MAX_NUM_ATTACKING_ARMIES);
		} else {
			if (attacker->getArmies()<MAX_NUM_ATTACKING_ARMIES) {
				// ComputerPlayer has less armies than the max allowed
				numOfAttackingArmies=attacker->getArmies();
			} else {
				// simply use the max allowed armies
				numOfAttackingArmies=MAX_NUM_ATTACKING_ARMIES;
			}
		}

		int numOfDefendingArmies;

		//Defender can only defend with at most 2 armies, according to risk rules.
		if (defender->getArmies()>=MIN_NUM_DEFENDING_ARMIE)
			numOfDefendingArmies=MIN_NUM_DEFENDING_ARMIE;
		else
			numOfDefendingArmies=defender->getArmies();

		view->battleStarting(attacker, defender, numOfAttackingArmies, numOfDefendingArmies);

		// Lists used to store the values of the dice rolled for the attacking / defending Countrys
		list<int> atk;
		list<int> def;

		Dice::rollDiceForPlayers(&atk, &def, numOfAttackingArmies, numOfDefendingArmies);

		while (atk.back()==def.back()) {
			view->displayStalemate(atk.back());
			atk.clear();
			def.clear();
			Dice::rollDiceForPlayers(&atk, &def, numOfAttackingArmies, numOfDefendingArmies);
		}

		if (atk.back() > def.back()) {
			Battle::defender->substractArmy(1);
			view->attackerRolledHigher(defender, atk.back(), def.back());
		} else {
			Battle::attacker->substractArmy(1);
			view->defenderRolledHigher(attacker, atk.back(), def.back());
		}

		// Check if there is a winner. If Attacker won, then he can conquer the defending Country.
		if (Battle::attacker->getArmies() == 0) {
			Battle::setStatus(true);
			view->defenderWonBattle();
			break;	//Exit while loop, ignore attacking variable.
		} else if (Battle::defender->getArmies() == 0) {
			Battle::setStatus(true);
			view->attackerWonBattle();
			Battle::conquer(numOfAttackingArmies);
			return true;
		}


		if (!allIn && typeid(*attacker->getPlayer())!= typeid(ComputerPlayer) && attacker->getArmies()!=1) {
			attacking = view->continueAttackingQuery(attacker, defender);
		} 

		if (attacker->getArmies()==1)
		{
			view->notEnoughArmies(attacker,defender);
			break;
		}

		if (typeid(*attacker->getPlayer())== typeid(ComputerPlayer)) {
			attacking=false;
		}

	}

	return false;
}

void Battle::setStatus(bool status) {
	Battle::status = status;
}

bool Battle::getStatus() {
	return Battle::status;
}

//Method that handles the conquering of countries. Requires the number of attacking armies as input, as this determines how many armies minimmum must be moved to the conquered country.
void Battle::conquer(int numOfAttackingArmies) {
	// verify that defender has no more armies on Country
	if (Battle::defender->getArmies() > 0) {
		return;
	}

	int movingArmies;
	if (typeid(*attacker->getPlayer())!= typeid(ComputerPlayer)) {
		// get the wanted number of moving armies from the player
		movingArmies = view->conquerQuery(attacker, defender, numOfAttackingArmies);
	} else {
		// for computer players...
		movingArmies=numOfAttackingArmies;
		while (movingArmies >= attacker->getArmies()) {
			movingArmies--;
		}
	}	


	transferCards(attacker, defender);

	// logic for conquering a country
	defender->getPlayer()->substractCountry(defender);
	defender->setPlayer(attacker->getPlayer());
	attacker->substractArmy(movingArmies);
	defender->addArmy(movingArmies);

	attacker->getPlayer()->addCard();
}

void Battle::transferCards(Country* attacker, Country* defender){

	for (int i=0; i< Player::numberOfCardTypes; i++)
	{
		if ((defender->getPlayer()->getCards()[i])  >0)
		{
			for (int j=0; j<= (defender->getPlayer()->getCards()[i]); j++)
			{

				attacker->getPlayer()->getCards()[i]++;
				defender->getPlayer()->getCards()[i]--;

			}
		}

	}
}

// Method used when the attacker wants to go all in. Calls doBattle until the attacker or defender runs out of armies.
void Battle::allIn() {
	while (Battle::attacker->getArmies() > 0 && Battle::defender->getArmies() > 0) {
		Battle::doBattle(true);
	}
}

// Default constructor
Battle::Battle() {
	// calling the default constructor should throw an exception as the the programmer must pass 2 adjacent Countrys
	throw std::invalid_argument("defender");
}

// Destructor
Battle::~Battle() {
}