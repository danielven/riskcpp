//
//  Continent.cpp
//

#include "stdafx.h"
#include "Continent.h"
#include "Country.h"
#include "Player.h"
#include "Map.h"
#include <algorithm>

//Generic Constructor
Country:: Country()
{
	_name="Default";
}

//Constructor
Country:: Country(string name, Continent* const &continent, Player* const &player)
{
	_name = name;
	_continent = continent;
	continent -> _countries.push_back(this);
	player -> addCountry(this);
	_player=player;
	_armies=0;

}

Country::Country(string name, Continent* const &continent){

	_name = name;
	_continent = continent;
	continent -> _countries.push_back(this);
	_armies=0;
}

Country:: Country(string name, Continent* const &continent, Player* const &player, int xCoord, int yCoord)
{
	_name = name;
	_continent = continent;
	continent -> _countries.push_back(this);
	player -> addCountry(this);
	_player=player;
	_armies=0;
	_coords.first = xCoord;
	_coords.second = yCoord;
}

Country::Country(string name, Continent* const &continent, int xCoord, int yCoord){
	_name = name;
	_continent = continent;
	continent -> _countries.push_back(this);
	_armies=0;
	_coords.first = xCoord;
	_coords.second = yCoord;
}

Country::Country(string name){
	_name=name;
	_armies=0;

}

void Country::setPlayer(Player* const &player){
	vector<Country*> countryvector = player->getCountryVector();
	// verify that country is not already in the Player's country vector
	vector<Country*>::iterator it= find(countryvector.begin(),countryvector.end(),this);
	if ((it)==countryvector.end()){
		player -> addCountry(this);
	}
	// set the country's owner to the new player
	_player=player;


	Notify();
}

void Country::addArmy(int arm)
{
	_armies+=arm;

	Notify();

}

void Country::substractArmy(int arm)
{
	if (arm<_armies)
		_armies-=arm;
	else
		_armies=0;


	Notify();

}

//Method that makes the countryA adjacent to another countryB, add the pointer to the countryB into the pointer array of adjacent countries of countryA. For each adjacent countryB make a pointer back to the countryA, add it into the pointer array of adjacent countries of countryB
void Country::makeAdjacent(Country* const &country)
{
	if (!isAdjacentTo(country))
	{_paths.push_back(country);
	country -> _paths.push_back(this);}
	Notify();
}


//Method that checks if an existing path exist with another country
bool Country::isAdjacentTo(Country* const &country)
{
	bool isAdjacent = false;
	for (vector<Country*>::iterator path_it = _paths.begin() ; path_it != _paths.end(); ++path_it){
		if(*path_it == country){
			isAdjacent = true;
			break;
		}
	}

	return isAdjacent;
}


vector<Country*> Country::getAdjacentCountries()
{

	return _paths;
}


Continent* Country::getContinent(){
	return _continent;
}

//getter method returns string name
string Country::getName()
{
	return _name;
}


Player* Country::getPlayer(){
	return _player;
}

//getter method returns int of number of connections
int Country::getNumberOfPaths()
{
	return _paths.size();

}

// Get and Set Armies


int Country::getArmies()
{
	return _armies;
}



bool Country::equal(Country* country2){

	return _name==country2->getName();
}

vector<Country*> Country::getAdjacentEnemyCountries(Player* const &player){

	vector<Country*> tempVector=this->getAdjacentCountries();

	vector<Country*>::iterator it=tempVector.begin();

	while (it!=tempVector.end())
	{
		if ((*it)->getPlayer()==player)
		{		
			if (it==prev(tempVector.end()))			
			{tempVector.erase(remove(tempVector.begin(),tempVector.end(),(*it)),tempVector.end());
			break;
			}
			else
				tempVector.erase(remove(tempVector.begin(),tempVector.end(),(*it)),tempVector.end());

		}
		else
			it++;
	}

	return tempVector;
}

vector<Country*> Country::getAdjacentAlliedCountries(Player* player){

	vector<Country*> tempVector=this->getAdjacentCountries();

	vector<Country*>::iterator it=tempVector.begin();

	while (it!=tempVector.end())
	{
		if ((*it)->getPlayer()!=player)
		{		
			if (it==prev(tempVector.end()))			
			{tempVector.erase(remove(tempVector.begin(),tempVector.end(),(*it)),tempVector.end());
			break;
			}
			else
				tempVector.erase(remove(tempVector.begin(),tempVector.end(),(*it)),tempVector.end());

		}
		else
			it++;
	}

	return tempVector;
}



const pair<int,int> Country::getCoords() {
	return _coords;
}

Country::~Country()
{

}

