#pragma once
#include "ComputerPlayer.h"
#include "Country.h"
#include <vector>
#include <set>
#include "Battle.h"

class Player;

class ComputerPlayer;

//In the future execute strategy could return a bool variable that states whether an attack happened or not. To be implemented in the future.

class Strategy
{
public:
	Strategy();
	virtual string getStrategyName() = 0;
	virtual void execute(ComputerPlayer*)=0;
	~Strategy();
};

