#include "stdafx.h"
#include "GameView.h"
#include <algorithm>
#include <sstream>


GameView::GameView(Game* game)
{
	_gameObserved=game;

	game->Attach(this);

}



void GameView::displaySubjectData(){

	const vector<Player*> players = _gameObserved->getPlayerVector();

	ConsoleUI::displaySeparatorLine();
}

void GameView::Update(){

	_console->cls();

	displaySubjectData();
}

//Inputs are name of the phase, and player executing it.
void GameView::displayStartOfPhase(string phase, Player* const player){

	printSeparatorLine();

	cout << phase;

	cout << " phase for player " << player->getPlayerID() << " (" << player->getName() << ")." << endl;

}

int GameView::redeemCardsMenu(Player* player, bool * options){

	cout << "You may now redeem cards for armies. Please choose one of the following options:" << endl;

	int i=0;

	int j=0;

	for (i; i<= Player::numberOfCardTypes-1; i++)
	{
		if (options[i]==true)
		{
			cout << ++j << ": You may redeem 3 " << Player::getCardName(i) << " cards (type " << j << ")." << endl;
		}

	}

	if (options[Player::numberOfCardTypes])
		cout << ++j << ": You may redeem 1 card of every type (type " << j << ")." <<endl;

	cout << ++j << ": You do not wish to redeem (type " << j << ")." << endl;

	int selection=0;

	cin.clear();

	string input;

	cin >> input;

	try {

		selection=stoi(input);
	}
	catch (...)
	{
		checkForMenuSelection(input,"reinforcement",player);
	}

	while (selection > j || selection<=0 || cin.fail())
	{

		cout << "You have entered an invalid selection. Please type an integer number greater than 0 and smaller than " << j << "." << endl;

		cin.clear(); 

		cin.ignore(numeric_limits<streamsize>::max(), '\n');

		cin >> input;

		try {

			selection=stoi(input);
		}
		catch (...)
		{
			checkForMenuSelection(input,"reinforcement",player);
			selection=0;
		}

	}

	for (int i=0; i <= Player::numberOfCardTypes ; i++)
	{
		if (options[i]==true)
		{
			selection--;

			if (selection==0)
				return i;
		}

	}

	return 4;
}

int GameView::selectMovingCountries(Country* fortifyingCountry, Country* fortitfiedCountry, string phase){

	cout << "How many armies will you move from " << fortifyingCountry->getName() << " (" << fortifyingCountry->getArmies() << ((fortifyingCountry->getArmies() > 1) ? " armies) " :" army) ") << " to " << fortitfiedCountry->getName() << " (" << ((fortifyingCountry->getArmies() > 1) ? " armies)?" :" army)?") << endl;

	cout << "Please type an integer number between 0 and " << fortifyingCountry->getArmies()-1 << " (remember you may not move all armies from " << fortifyingCountry->getName() <<". Type 0 if you wish to reinforce from another country:" << endl;


	int selection=fortifyingCountry->getArmies() + 1;

	string input;

	cin >> input;

	try {

		selection=stoi(input);
	}
	catch (...)
	{
		checkForMenuSelection(input,phase,fortifyingCountry->getPlayer());
	}


	while (selection > fortifyingCountry->getArmies() || selection<0 || cin.fail())
	{

		cout << "You have entered an invalid selection. Please type an integer number between 0 and " << fortifyingCountry->getArmies()-1 << "." << endl;

		cin.clear(); 

		cin.ignore(numeric_limits<streamsize>::max(), '\n');

		cin >> selection;
	}

	return selection;
}

Country* GameView::reinfocePlayerMenu(Player* player, int addedArmies, string phase){

	cout << "Player  " << player->getPlayerID() << " (" << player->getName() << ") may now add " << addedArmies << ((addedArmies > 1) ? " armies" :" army.") << endl;

	bool result = false;

	cin.clear();

	cin.ignore(numeric_limits<streamsize>::max(), '\n'); 

	cout << "Available countries are: " << endl;

	PlayerView::printCountriesAndArmies(player);

	cout << "Type a number to add armies to this country:" <<endl;
	size_t selection =0;

	string input;

	cin >> input;

	try {

		selection=stoi(input);
	}
	catch (...)
	{
		checkForMenuSelection(input,phase,player);
	}


	while (selection > player->getCountryVector().size() || selection<1 || cin.fail())
	{

		cout << "You have entered an invalid selection. Please type an integer number greater than 0 and smaller than " << player->getCountryVector().size() << "." << endl;

		cin.clear(); 

		cin.ignore(numeric_limits<streamsize>::max(), '\n');

		cin >> input;

		try {

			selection=stoi(input);
		}
		catch (...)
		{
			checkForMenuSelection(input,phase,player);
			selection =0;
		}
	}

	return player->getCountryVector()[selection-1];


}

int GameView::addArmyToCountryMenu(Country* country, int addedArmies, string phase){


	cout << "How many armies will you add at " << country->getName() << "? Maximum " << addedArmies << endl;

	unsigned int selection = addedArmies-1;

	string input;

	cin >> input;

	try {

		selection=stoi(input);
	}
	catch (...)
	{
		checkForMenuSelection(input,phase, country->getPlayer());
	}


	while (selection > addedArmies || selection<0 || cin.fail())
	{

		cout << "You have entered an invalid selection. Please type an integer number greater than 0 and smaller than " << addedArmies << "." << endl;

		cin.clear(); 

		cin.ignore(numeric_limits<streamsize>::max(), '\n');

		cin >> input;

		try {

			selection=stoi(input);
		}
		catch (...)
		{
			checkForMenuSelection(input,phase, country->getPlayer());
			selection=addedArmies-1;
		}

	}

	return selection;

}

Country* GameView::selectAttackingCountryMenu(Player* player, vector<Country*> availableCountriesForAttack){

	if (availableCountriesForAttack.empty())
	{
		cout <<"Player " << player->getPlayerID() << " (" << player->getName() << ") has no available countries to attack from." << endl;
		return nullptr;
	}

	return queryUserForCountry(availableCountriesForAttack,"attack from","attack");


}

Country* GameView::selectDefendingCountryMenu(Player* player, vector<Country*> availableCountriesToAttack){

	if (availableCountriesToAttack.empty())
	{

		return nullptr;
	}

	return queryUserForCountry(availableCountriesToAttack,"attack","attack");
}

Country* GameView::selectFortifyingCountryMenu(Player* player, vector<Country*> availabeCountriesForReinforcing){

	if (availabeCountriesForReinforcing.empty())
	{
		cout <<"Player " << player->getPlayerID() << " (" << player->getName() << ") has no available countries to reinforce from." << endl;
		return nullptr;
	}

	return queryUserForCountry(availabeCountriesForReinforcing,"reinforce from","fortification");


}

Country* GameView::selectCountryToFortifyMenu(Player* player, vector<Country*> availableCountriesToFortify){

	if (availableCountriesToFortify.empty())
	{
		cout << "Error. No available contries to reinforce.";
		return nullptr;
	}

	return queryUserForCountry(availableCountriesToFortify,"reinforce","fortification");

}

Country* GameView::queryUserForCountry(vector<Country*> countryVector, string action, string phase){

	if (countryVector.empty())
		return nullptr;

	cout << "Available countries to " << action << " are: " << endl;

	if (action=="attack")
		PlayerView::printFullCountryDetails(countryVector);
	else
		PlayerView::printCountriesAndArmies(countryVector);

	cout << "Choose a country to " << action << ", or type No if you do not wish to perform this action:" <<endl;

	string input;

	cin.clear();

	cin >> input;

	checkForMenuSelection(input,phase, countryVector.front()->getPlayer());

	transform(input.begin(), input.end(), input.begin(), ::tolower);

	size_t intString=0; //signed unsigned mismatch

	if (string(input) !="no")
	{
		try
		{
			intString= stoi(input);		
		}
		catch (...) {

			checkForMenuSelection(input,phase,countryVector.front()->getPlayer());
		}

		while (intString < 1 || intString > countryVector.size())
		{		

			cout << "You have entered an invalid input. Please type an integer number between than 1 and  " << countryVector.size() << ", or type no if you do not wish to attack." << endl;

			cin.clear(); 

			cin.ignore(numeric_limits<streamsize>::max(), '\n');

			cin >> input;

			transform(input.begin(), input.end(), input.begin(), ::tolower);

			if (string(input) =="no")
				break;

			try
			{
				intString= stoi(input);		
			}
			catch (...) {

				checkForMenuSelection(input,phase,countryVector.front()->getPlayer());
			}

		}

	}

	if (string(input)=="no")
		return nullptr;

	return countryVector[stoi(input)-1];

}

void GameView::printEndOfGameMessage(Player* player){

	printLongSeparatorLine();

	try
	{
		cout << "CONGRATULATIONS " << player->getName() << " YOU ARE THE CONQUEROR OF THE WORLD!" << endl;
	}
	catch (std::runtime_error)
	{
		cout << "Error ocurred with victory condtition" << endl;
	}


}

string GameView::queryUserForFileName(){

	cout << "You have elected to save this game. Please enter a name for your file, preferably a .txt:" << endl;

	cin.ignore();

	string input;

	cin >> input;

	return input;

}

//checks whether the user has inputted a menu option, and handles it from the game. The inputs are user input, current phase, current player.
void GameView::checkForMenuSelection(string input, string phase,Player* player){

	transform(input.begin(), input.end(), input.begin(), ::tolower);

	if (input=="savegame" || input=="exit")
		_gameObserved->handleMenuInput(input, phase, player);

}

bool GameView::queryUserForContinuingAttack(Player* player){

	stringstream message;

	message << "Will player " << player->getName() << " (" << player->getName() << ") continue attacking?";

	return yesOrNoQuery(message.str());

}

int GameView::queryForHumanPlayers(int max_players){

	int num_of_players;

	cin.clear();
	cin.ignore(numeric_limits<streamsize>::max(), '\n'); 

	cout << "How many human players for this session? From 1 to 6 players." <<endl;
	cin >> num_of_players;


	while (num_of_players < 1 || num_of_players > max_players || cin.fail())
	{
		cout << "You have entered an invalid input. Please enter a integer number between 1 and 6." << endl;
		if (cin.fail())
		{
			cin.clear(); 
			cin.ignore(numeric_limits<streamsize>::max(), '\n'); 
			cout << "Please enter an Integer only." << endl;
		}

		cin >> num_of_players;

	}

	return num_of_players;
}


///Inputs are minimum allowed players, max allowed playes, and Human players so far.
int GameView::queryForAIPlayers(int minNumber, int max_players, int num_of_players){

	cout << "How many AI players for this session? From " << minNumber << " to " << max_players - num_of_players <<" players." <<endl;

	int num_of_AI_players;

	cin >> num_of_AI_players;

	cin.clear();


	while (num_of_AI_players > max_players-num_of_players || cin.fail()  || num_of_AI_players<minNumber)
	{
		cout << "You have entered an invalid input. Please enter a integer number between " << minNumber << " and " << max_players - num_of_players <<" players." << endl;
		if (cin.fail())
		{
			cin.clear(); 
			cin.ignore(numeric_limits<streamsize>::max(), '\n'); 
			cout << "Please enter an Integer only." << endl;
		}

		cin >> num_of_AI_players;

	}

	return num_of_AI_players;

}

GameView::~GameView(void)
{
	_gameObserved->Detach(this);
}
