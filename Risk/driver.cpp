#include "stdafx.h"


#include "Game.h"
#include <iostream>
#include "Player.h"
#include "ComputerPlayer.h"
#include "Continent.h"
#include "Map.h"
#include "Country.h"
#include "PlayerView.h"
#include "Battle.h"
#include "AggressiveStrat.h"
#include "DefensiveStrat.h"
#include "RandomStrat.h"
#include "GameBuilder.h"
#include "PlayerView.h"
#include "ConsoleLogger.h"
#include "HumanPlayer.h"
#include "CorruptedSaveFile.h"
#include "BasicStats.h"
#include "MapView.h"
#include "MapEditor.h"
#include <sstream>
#include <time.h>
#include <stdexcept>
#include <typeinfo>
#include <Windows.h>


using namespace std;


int main(){


	// Game start options

	cout << "Welcome to Risk! by Jesus Imery, Daniel Ventulieri, and Justin Jiabing Ying." << endl;
	cout << "To start a game, you can either opt to create a New Game, or load a game from a save file." << endl;
	cout << "Type new for a new game,load to load a game or editor to test the map editor." << endl;

	string option;

	cin >> option;

	while (option!= "new" && option != "load" && option!="new game" && option!="load game" && option!="map editor" && option!="editor") {
		cout << "You have entered an invalid command." << endl;
		cout << "Type new for a new game, load to load a game or editor to test the map editor." << endl;

		option = "";

		cin >> option;

		transform(option.begin(), option.end(), option.begin(), ::tolower);

	}

	// create a new game
	if (string(option) == "new" || string(option) == "new game") {

		cout << "Beginning game!!" << endl;

		Map map;

		map.genericMapCreator();

		Game game(&map);

		game.initializePlayers();

		game.randomlyAssignArmies();

		game.startGame();

	}
	else if (string(option) == "load" || string(option) == "load game") {
		string fileName = "";
		cout << "Load file initiated. Which file do you want to load?" << endl;
		cout << "format: <filename>.txt" << endl;
		cin >> fileName;

		GameBuilder builder;

		builder.createNewGame(string(fileName));

		builder.buildMap();

		builder.buildPlayers();

		builder.attachMapToGame();

		Game* game = builder.getGame();

		builder.startGameFromTurn();

	}
	else if (string(option) == "editor" || string(option) == "map editor")
	{

		cout << "The map editor was not finished, since it was not stated in the project description. However, you may see the general outline of the intended functionality, and add countries to the sample map, observing how the viewer class updates properly" << endl;

		GameBuilder game;

		game.createNewGame("test.txt");

		game.buildMap();

		Map* map = game.getMap();

		map->removeCountry(map->getCountryVector().front());

		map->removeContinent(map->getContinentVector().back());

		MapEditor* mapeditor= new MapEditor();

		mapeditor->loadMap(map);

		mapeditor->beginConstruction();

		delete mapeditor;


	}


	system("PAUSE");



	return 0;

}

