//
//  Country.h
//  Header file for Country.cpp
//
#pragma once;
#ifndef COUNTRY_H_
#define COUNTRY_H_


#include "Continent.h"
#include "Map.h"
#include "Observable.h"
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Player;

class Country : public Observable {


    
public:
    Country(string name, Continent* const &continent, Player* const &player);
	Country(string name, Continent* const &continent);
	Country(string name, Continent* const &continent, Player* const &player, int xCoord, int yCoord);
	Country(string name, Continent* const &continent, int xCoord, int yCoord);
	Country(string name);
	
	void setPlayer(Player* const &player);
	void substractArmy(int);
	void addArmy(int);

    string getName();
    int getNumberOfPaths();
    int getArmies();
	Continent* getContinent();
	vector<Country*> getPaths() {return _paths;};
	Player* getPlayer();
	const pair<int,int> getCoords();
    bool isAdjacentTo(Country* const &country);
	void setContinent(Continent* continent) {_continent=continent;};
	void makeAdjacent(Country* const &country); 
	vector<Country*> getAdjacentCountries();
	vector<Country*> getAdjacentEnemyCountries(Player* const &);
	vector<Country*> getAdjacentAlliedCountries(Player*);

	bool equal(Country* country2);
		
	Country();
    ~Country();
    
private:
    string _name;
    vector<Country*> _paths;
    Continent* _continent;
    int _armies;
    Player* _player;
	pair<int, int> _coords;



	   
};




#endif