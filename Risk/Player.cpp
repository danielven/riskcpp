
#include "stdafx.h"

#include "Player.h"
#include "Country.h"
#include <vector>
#include <iostream>
#include <sstream>
#include <time.h>
#include <algorithm>

using namespace std;

static const string cardNameArray[]={"Infantry","Artillery","Cavalry"};
const string* Player::cardNames=cardNameArray;

//Generic constructor
Player::Player() {
	countriesOwned = 0;
	for (int i=0; i<numberOfCardTypes; i++)
		ownedCards[i]=0;
	numberOfRedemptions=0;
}

Player::Player(string newName, int id) {

	playerID= id;
	countriesOwned = 0;
	name=newName;
	for (int i=0; i<numberOfCardTypes; i++)
		ownedCards[i]=0;
	numberOfRedemptions=0;
}


string Player::getName(){

	return name;
}

void Player::addCountry(Country* const &addedCountry){

	Player::countryVector.push_back(addedCountry);
	Player::countriesOwned++;
	//addedCountry->setPlayer(this);
	//Update continent set if continent has just been completely conquered.
	updateContinentSet(addedCountry->getContinent());

	Notify();
}

//Adds continent to continentSet when this player owns all countries in said continent.
void Player::updateContinentSet(Continent* continent){
	vector<Country*> tempVector=continent->getCountries();
	bool result=true;
	for (vector<Country*>::iterator it = tempVector.begin(); it != tempVector.end(); ++it) 
		if ((*it)->getPlayer()!=this){
			continentSet.erase(continent);
			result=false;
			break;}

		if (result==true){
			continentSet.emplace(continent);
		}

		Notify();
}

int Player::getCountriesOwned(){
	return countriesOwned;
}

int Player::getPlayerID(){
	return playerID;
}

vector<Country*> Player::getCountryVector()
{
	return countryVector;

}


//Returns a set of all enemy countries that are adjacent to countries owned by this player.
set<Country*> Player::getAdjacentEnemyCountries()
{

	set<Country*> adjacentContries;

	for (vector <Country*>::iterator it= countryVector.begin(); it != countryVector.end(); it++)
	{

		for (vector <Country*>::iterator it2= (*it)->getAdjacentCountries().begin(); it2 != (*it)->getAdjacentCountries().end(); it2++)
		{

			if ((*it2)->getPlayer()!=this)
				adjacentContries.insert(*it2);
		}
	}

	return adjacentContries;
}

//Returns a set of countries that are owned by this player and that have adjacent enemy countries with less armies.
set<Country*> Player::getStrongCountries(){

	set<Country*> strongCountries;


	for (vector <Country*>::iterator it= this->countryVector.begin(); it != this->countryVector.end(); it++)
	{

		Country* con= *it;

		vector<Country*> vector2=con->getAdjacentCountries();

		for (vector <Country*>::iterator it2= vector2.begin(); it2 != vector2.end(); it2++)
		{

			Country* adjCon= *it2;

			if (con->getArmies() > adjCon->getArmies() && adjCon->getPlayer()!=con->getPlayer())
				strongCountries.insert(*it);


		}

	}

	return strongCountries;

}

//Returns a set of countries that are owned by an enemy player and that have adjacent countries owned by the (this) player with more armies.
set<Country*> Player::getWeakEnemyCountries(){

	set<Country*> weakCountries;


	for (vector <Country*>::iterator it= this->countryVector.begin(); it != this->countryVector.end(); it++)
	{

		Country* con= *it;

		vector<Country*> vector2=con->getAdjacentCountries();

		for (vector <Country*>::iterator it2= vector2.begin(); it2 != vector2.end(); it2++)
		{

			Country* adjCon= *it2;

			if (con->getArmies() > adjCon->getArmies() && adjCon->getPlayer()!=con->getPlayer())
				weakCountries.insert(*it2);

		}

	}
	return weakCountries;
}

//Returns a map where the keys are countries with at least one adjacent enemy country with less armies. Mapped values are the set of countries adjacent to the key value that satisfy this condition. Mainly used for aggressive strategy.
map<Country*, set<Country*>> Player::getStrongCountryMap()
{

	map<Country*,set<Country*>> strongCountryMap;

	for (vector <Country*>::iterator it= this->countryVector.begin(); it != this->countryVector.end(); it++)
	{

		Country *con= *it;

		set<Country*> weakCountries;

		vector<Country*> vector2=con->getAdjacentCountries();

		bool hit=false;

		for (vector <Country*>::iterator it2= vector2.begin(); it2 != vector2.end(); it2++)
		{

			Country* adjCon= *it2;

			if (con->getArmies() > adjCon->getArmies() && adjCon->getPlayer()!=con->getPlayer())
			{
				weakCountries.insert(*it2);
				hit=true;
			}


		}



		if (hit)
			strongCountryMap.emplace(con, weakCountries);


	}

	return strongCountryMap;

}

//Returns a map where the key values are countries belonging to this player, and the mapped values are sets of enemy countries adjacent to the keys. Mainly used for random strategy.
map<Country*, set<Country*>> Player::getCountryMap()
{

	map<Country*,set<Country*>> countryMap;

	for (vector <Country*>::iterator it= this->countryVector.begin(); it != this->countryVector.end(); it++)
	{
		set<Country*> weakCountries;

		bool hit=false;

		vector<Country*> adjacentCountryVector= (*it)->getAdjacentCountries();

		for (vector <Country*>::iterator it2= adjacentCountryVector.begin(); it2 != adjacentCountryVector.end(); it2++)
		{


			if ((*it2)->getPlayer()!=(*it)->getPlayer())
			{
				weakCountries.insert(*it2);
				hit=true;
			}


		}



		if (hit)
			countryMap.emplace((*it), weakCountries);


	}

	return countryMap;

}



//Returns true when the inputted country is owned by the player, and all enemy adjacent countries have less armies than his. If there are no adjacent enemy countries it returns false. Used for rechecking whether this player should attack during defensive strategy.
bool Player::checkForAllWeakAdjacent(Country* playersCountry){

	if (playersCountry->getPlayer()!=this)
		return false;

	bool condition=true;

	bool condition2=false;

	vector<Country*> adjacentCountryVector=playersCountry->getAdjacentCountries();

	for (vector<Country*>::iterator it=adjacentCountryVector.begin(); it!=adjacentCountryVector.end(); it++)
	{

		if ((*it)->getPlayer()!=playersCountry->getPlayer())
			condition2=true;

		if ((*it)->getArmies()>=playersCountry->getArmies() && (*it)->getPlayer()!=playersCountry->getPlayer())
		{condition=false;
		}
	}

	return condition && condition2;

}

//Returns a set of countries owned by this player where all its enemy adjacent countries have less armies than his. Used for defensive strategy.
set<Country*> Player::getStrongDefensiveCountrySet()
{

	set<Country*> strongDefensiveCountrySet;

	for (vector<Country*>::iterator it=countryVector.begin(); it!=countryVector.end(); it++)
	{

		bool condition=true;

		vector<Country*> adjacentCountryVector=(*it)->getAdjacentCountries();

		for (vector<Country*>::iterator it2=adjacentCountryVector.begin(); it2!=adjacentCountryVector.end(); it2++)

		{
			if ((*it2)->getArmies()>=(*it)->getArmies() && (*it2)->getPlayer()!=(*it)->getPlayer())
				condition=false;
		}

		if (condition==true)
			strongDefensiveCountrySet.insert(*it);

	}

	return strongDefensiveCountrySet;

}

void Player::substractCountry(Country* substractedCountry){

	vector<Country*>::iterator it= countryVector.begin();

	while (it!= countryVector.end())
	{
		if ((*it)==substractedCountry)
		{
			it=countryVector.erase(it);
			break;
		}

		it++;
	}
	countriesOwned--;

	//Delete contient from set if previously owned.
	if (bool is_in = continentSet.find(substractedCountry->getContinent()) != continentSet.end())
		continentSet.erase(substractedCountry->getContinent());

	Notify();

}



Player::Card Player::makeRandomCard(){

	int i=rand() % 3;

	Card card;

	if (i==0)
		card=INFANTRY;
	else if (i==1)
		card=CAVALRY;
	else if (i==2)
		card=ARTILLERY;

	return card;

}

string Player::getCardName(int enumInt){

	if (enumInt>2 || enumInt<0)
	{
		return "";
	}

	return Player::cardNames[enumInt];

}

void Player::addCard(){

	Card card=makeRandomCard();

	ownedCards[card]++;

	Notify();
}

void Player::addCard(int i){
	if (i>=0 && i<numberOfCardTypes)
		ownedCards[i]++;

	Notify();
}


int Player::getTotalNumberOfCards(){

	int total=0;

	for (int i=0; i<numberOfCardTypes; i++)
		total+=ownedCards[i];

	return total;
}

void Player::substractCard(int i){

	ownedCards[i]--;

	Notify();

}

int Player::getNumberOfArmies() {
	int numberOfArmies = 0;
	for (vector<Country*>::iterator it = countryVector.begin(); it != countryVector.end(); ++it) {
		numberOfArmies += (*it)->getArmies();
	}
	return numberOfArmies;
}

set<Continent*> Player::getContinentSet(){

	return continentSet;

}

int * Player::getCards(){
	int * cards;
	cards=ownedCards;
	return cards;
}

int Player::getRedemptions(){
	return numberOfRedemptions;
}

void Player::addRedemption(){
	numberOfRedemptions++;
	Notify();
}



Player::~Player() {
}

