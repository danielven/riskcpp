#include "stdafx.h"
#include "BattleView.h"
#include <string>
#include <iostream>
#include <sstream>
using namespace std;

BattleView::BattleView(void)
{
}

// Display the error that the attacker does not have enough armies to attack the country
void BattleView::notEnoughArmies(Country* attacker, Country* defender) {
	cout << "Player " << attacker->getPlayer()->getPlayerID() << " (" << attacker->getPlayer()->getName() << ") may not attack Player " << defender->getPlayer()->getPlayerID() << " (" << defender->getPlayer()->getName() << "): from " << attacker->getName() << " since it has just 1 army." <<endl;
}

void BattleView::displayAttackerInfo(Country* attacker) {
	cout << "Player " << attacker->getPlayer()->getPlayerID() << " (" << attacker->getPlayer()->getName() << ") is attacking from " << attacker->getName() << "." << endl;
	cout << "Armies for player " << attacker->getPlayer()->getPlayerID() << " (" << attacker->getPlayer()->getName() << "): " << attacker->getArmies() << endl;
}

void BattleView::displayDefenderInfo(Country* defender) {
	cout << "Player " << defender->getPlayer()->getPlayerID() << " (" << defender->getPlayer()->getName() << ") is defending from " << defender->getName() << "." << endl;
	cout << "Armies for player " << defender->getPlayer()->getPlayerID() << " (" << defender->getPlayer()->getName() << "): " << defender->getArmies() << endl;
}

int BattleView::getNumberOfAttackingArmies(Country* attacker, const int maxNumberOfArmies) {
	int numOfAttackingArmies;
	cout << "How many armies will player " << attacker->getPlayer()->getPlayerID() << " (" << attacker->getPlayer()->getName() << ") attack with?" << endl;

	cin.clear();

	cin >> numOfAttackingArmies;

	// verify that the input is correct.
	while (numOfAttackingArmies >= attacker->getArmies() || cin.fail() || numOfAttackingArmies > maxNumberOfArmies || numOfAttackingArmies<=0) {
		if (numOfAttackingArmies >= attacker->getArmies()) {
			cout << "You have entered an invalid input. You may not use all your armies to attack (must leave back at least one army)." << endl;
		}

		if (numOfAttackingArmies > maxNumberOfArmies) {
			cout<< "You have entered an invalid input. You may attack with at most " << maxNumberOfArmies << " armies." << endl;
		}

		if (numOfAttackingArmies == 0)
			cout<< "You have entered an invalid input. You cannot attack with 0 armies." << endl;;

		if (cin.fail()) {
			cin.clear(); 
			cin.ignore(numeric_limits<streamsize>::max(), '\n'); 
			cout << "Please enter an Integer only." << endl;
		}
		numOfAttackingArmies = attacker->getArmies();
		cin >> numOfAttackingArmies;
	}

	return numOfAttackingArmies;
}

void BattleView::battleStarting(Country* attacker, Country* defender, int numOfAttackingArmies, int numOfDefendingArmies) {
	cout << "Attacking player " << attacker->getPlayer()->getPlayerID() << " (" << attacker->getPlayer()->getName() << ") will attack with " << numOfAttackingArmies << " armies." <<endl;
	cout << "Defending player " << defender->getPlayer()->getPlayerID() << " (" << defender->getPlayer()->getName() << ") will defend with " << numOfDefendingArmies << " armies." <<endl;
}

void BattleView::displayStalemate(int rolled) {
	cout << "Stalemate. Both countries rolled " << rolled << ". Rerolling." << endl;
}

void BattleView::attackerRolledHigher(Country* defender, int attackerRoll, int defenderRoll) {
	cout << "Attacking Country rolled " << attackerRoll << endl;
	cout << "Defending Country rolled " << defenderRoll << endl;
	cout << "Defending Country loses. Now has " << defender->getArmies() << " armies left." << endl;
}

void BattleView::defenderRolledHigher(Country* attacker, int attackerRoll, int defenderRoll) {
	cout << "Attacking Country rolled " << attackerRoll << endl;
	cout << "Defending Country rolled " << defenderRoll << endl;
	cout << "Attacking Country loses. Now has " << attacker->getArmies() << " armies left." << endl;
}

void BattleView::defenderWonBattle() {
	cout << "Attacker has no more armies and lost the battle" << endl;
}

void BattleView::attackerWonBattle() {
	cout << "Defender has no more armies. Attacker may now conquer the Country" << endl;
}

// Query to see if player wants to continue attacking
bool BattleView::continueAttackingQuery(Country* attacker, Country* defender) {

	stringstream message;
	message << "Will player " << attacker->getPlayer()->getPlayerID() << " (" << attacker->getPlayer()->getName() << ") continue attacking Player " << defender->getPlayer()->getPlayerID() << " (" << defender->getPlayer()->getName() << ", " << defender->getName() <<") from " << attacker->getName() <<"?";
	return yesOrNoQuery(message.str());

}

// Query for seeing how many armies the conquerer wants to move into to the newly conquered country
int BattleView::conquerQuery(Country* attacker, Country* defender, int numOfAttackingArmies) {
	int movingArmies;
	cout << "How many armies will player " << attacker->getPlayer()->getPlayerID() << " (" << attacker->getPlayer()->getName() << ") move to " << defender->getName() << "?" << " Minumum of " << numOfAttackingArmies << "." << endl;
	cin >> movingArmies;

	if (typeid(movingArmies).name()!="int") {
		static_cast<int>(movingArmies);
	}

	while (movingArmies < numOfAttackingArmies || movingArmies>=attacker->getArmies() || cin.fail())
	{

		if (movingArmies < numOfAttackingArmies)
			cout << "You have entered an invalid input. You must move at least as many armies as those you attacked with (" << numOfAttackingArmies<< ")." << endl;

		if (movingArmies >= attacker->getArmies())
			cout << "You have entered an invalid input. You must leave at least one army back in " << attacker->getName() <<" (armies in " <<attacker->getName() << ": " << attacker->getArmies() <<")." << endl;

		if (cin.fail())
		{
			cin.clear(); 
			cin.ignore(numeric_limits<streamsize>::max(), '\n'); 
			cout << "Please enter an Integer only." << endl;
		}
		movingArmies=attacker->getArmies()+10;
		cin >> movingArmies;

	}
	return movingArmies;
}

BattleView::~BattleView(void)
{
}
