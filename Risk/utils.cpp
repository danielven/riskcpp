#include "stdafx.h"
#include "utils.h"
#include <cstdlib>
#include <ctime>
utils::utils()
{
}

int utils::rollDice() {
	// returns random number between 1-6 inclusively
	return rand() % 6 + 1;
}

void utils::sort(int a[], int size) {
	int indexOfNextLargest;
	for (int i = 0; i < size - 1; i++) {
		// Place the correct value in a[i]
		indexOfNextLargest = indexOfLargest(a, i, size);
		swapValues(a[i], a[indexOfNextLargest]);
	}
}

void utils::swapValues(int& v1, int& v2) {
	int temp;
	temp = v1;
	v1 = v2;
	v2 = temp;
}

int utils::indexOfLargest(const int a[], int startIndex, int size) {
	int max = a[startIndex], indexOfMax = startIndex;
	for (int i = startIndex; i < size; i++) {
		if (a[i] > max) {
			max = a[i];
			indexOfMax = i;
		}
	}
	return indexOfMax;
}

// Fill the lists with rolled dice
void utils::rollDiceForPlayers(list<int>* atk, list<int>* def, int numOfAttackingArmies, int numOfDefendingArmies)
	{
	for (int i = 0; i < numOfAttackingArmies; i++)
	{

		atk->push_back(utils::rollDice());
	}

	for (int i = 0; i < numOfDefendingArmies; ++i)
	{
		def->push_back(utils::rollDice());
	}
}

utils::~utils()
{
}