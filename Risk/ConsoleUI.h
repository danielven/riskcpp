#pragma once
#include "ConsoleLogger.h"
#include <string>

class ConsoleUI
{
	public:
	virtual void closeDisplay();
	virtual void display();						//Creates and shows the display window
	virtual void display(std::string);			//Creates and shows the display window with a title
	virtual bool yesOrNoQuery(std::string);

protected:

	const static int secondColumnDisplayPosition=30;
	const static int endOfDisplayPosition=70;
	CConsoleLoggerEx* _console;


	ConsoleUI(void);
	static const char*  _genericConsoleTitle;
	void displaySeparatorLine();
	static void printSeparatorLine();
	static void printLongSeparatorLine();
	virtual void displaySubjectData()=0;
	~ConsoleUI(void);
};

