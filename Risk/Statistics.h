#pragma once
#include "Observer.h"
#include "Player.h"
using namespace std;

class Statistics : public Observer
{
public:
	// Either display the statistics of all players, or of a certain player (by passing the player's ID)
	virtual void getStats() = 0;
	virtual void observe(Player* player) = 0;
	virtual void observe(Game* game) = 0;
};


