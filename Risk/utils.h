#pragma once
#include <list>
using namespace std;

class utils
{
public:
	utils();
	// method to simulate a dice roll
	static int rollDice();
	// selection sort method: found in lectures notes (COMP345.5.arrays.ppt)
	static void sort(int a[], int size);
  // Used in sort method. Also may be useful for quickly swapping values in an array
	static void swapValues(int& v1, int& v2);
  // finds the index of the largest value in an array, used in Sort method
	static int indexOfLargest(const int a[], int startIndex, int size);
	
	// Fill the lists with rolled dice
	static void rollDiceForPlayers(list<int>*, list<int>*, int, int);

	~utils();
};