#pragma once
#include <exception>
#include <string>
#include <iostream>
using namespace std;

class CorruptedSaveFile : public exception {
public:
	static enum Type {CARD, PHASE, STRATEGY, SAVE};
	CorruptedSaveFile(Type t);
	void handle();
	~CorruptedSaveFile(void);
private: 
	Type type;
};

