#include "stdafx.h"
#include <sstream>
#include "BasicStats.h"

// BasicStats constructor takes a game parameter in its constructor. It will track the stats of the given game.
BasicStats::BasicStats(vector<Player*> players) {
	for (size_t i = 0; i < players.size(); ++i) {
		Player* p = players[i];
		observe(p);
	}
}


// creates and returns the string of all the stats

void BasicStats::getStats() {
	_console->printf("Players in the game: \n");
	displaySeparatorLine();	
	_console->printf(getPlayersAndArmies().c_str());
}
/*
Add the player to the list of subjects. When the player updates itself, the stats will be updated.
Adds the player to the maps of stats (countries controlled, armies owned, cards owned)
*/
void BasicStats::observe(Player* player) {
	player->Attach(this);
	// add to list of subjects
	_subjects.push_back(player);
	for (size_t i = 0; i < player->getCountryVector().size(); ++i) {
		Country* c = player->getCountryVector()[i];
		c->Attach(this);
	}
}	

/*
Alternatively, the observe method can be passed a Game pointer. 
It will them observe every player in the game
*/
void BasicStats::observe(vector<Player*> players) {
	for (int i = 0; players.size(); ++i) {
		Player* p = players[i];
		observe(p);
	}
}

void BasicStats::Update() {
	_console->cls();
	displaySubjectData();
}


string BasicStats::getPlayersAndArmies(){

	stringstream result;

	list<Player*>::iterator it = _subjects.begin();
	for (; it != _subjects.end(); ++it)
	{
		stringstream stats;
		stats << "Player ";
		stats <<(*it)->getPlayerID();
		stats <<" (";
		stats<<(*it)->getName();
		stats<<")";
		stats.seekg(0, ios::end);
		int i=0;
		i += static_cast<int>(stats.tellg());
		for (i;i<ConsoleUI::secondColumnDisplayPosition; i++)
			stats<<" ";
		stats<<(*it)->getNumberOfArmies();
		stats<<(((*it)->getNumberOfArmies() ==1 ) ? " army" : " armies");
		stats.seekg(0, ios::end);
		i=static_cast<int>(stats.tellg())-i;
		for (i;i<ConsoleUI::secondColumnDisplayPosition-10; i++)
			stats<<" ";
		stats.seekg(0, ios::end);
		i=static_cast<int>(stats.tellg())-i;
		for (i;i<ConsoleUI::secondColumnDisplayPosition-15; i++)
			stats << " ";
		stats << (*it)->getCountriesOwned();
		stats << (((*it)->getCountriesOwned() ==1 ) ? " country" : " countries");
		stats.seekg(0, ios::end);
		i=static_cast<int>(stats.tellg())-(i*2);
		for (i;i<ConsoleUI::secondColumnDisplayPosition-20; i++)
			stats << " ";
		stats << (*it)->getTotalNumberOfCards();
		stats << (((*it)->getTotalNumberOfCards() ==1 ) ? " card" : " cards");
		stats << "\n";
		result << stats.str();
	}

	return result.str();
}

void BasicStats::displaySubjectData(){

	_console->printf("RISK, THE ULTIMATE WORLD DOMINATION GAME!!!\n");

	displaySeparatorLine();

	getStats();

	displayMenuOptions();

}

void BasicStats::displayMenuOptions(){

	displaySeparatorLine();

	_console->printf("You may type one of the following commands when outside of a battle to perform the action desired:\n");

	_console->printf("*Savegame\n");

	_console->printf("*Exit\n");

}

BasicStats::BasicStats() {
}
BasicStats::~BasicStats()
{
}
