//
//  Continent.h
//  Header file for Continent.cpp
//


#ifndef CONTINENT_H_
#define CONTINENT_H_

# include <iostream>
# include <string>
# include <vector>

#include "Country.h"

using namespace std;

class Continent {
    
    friend class Country;
    
public:
    Continent(string, int);
	bool equal(Continent* continent2);
	bool addCountry(Country*);
    ~Continent();
    string getName();
    void printCountries();
	bool countryExist(Country country);
	vector<Country*> getCountries();   
	int getBonus();
    
private:
    string _name;
    vector<Country*> _countries;
	int bonus;
    
    
};
#endif