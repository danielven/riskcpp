#include "stdafx.h"
#include "GameBuilder.h"
#include "AggressiveStrat.h"
#include "DefensiveStrat.h"
#include "RandomStrat.h"
#include "CorruptedSaveFile.h"

GameBuilder::GameBuilder(void)
{
}


void GameBuilder::createNewGame(string str){
	game= new Game;
	map= new Map;
	fileName=str;
}

void GameBuilder::buildMap(){

	map->createMapWithArmies(fileName);

}

void GameBuilder::buildPlayers(){

	int i=1;

	ifstream file(fileName);

	char word[256];

	while (string(word)!="Players,")
		file.getline(word,256);

	file.getline(word,256,',');

	while (string(word)!="Cards")
	{

		string name=word;

		file.getline(word,256,',');

		HumanPlayer* player= new HumanPlayer(name,i);						

		ComputerPlayer * aiPlayer= new ComputerPlayer(name,i);

		++i;

		bool ai=false;

		if (string(word)!="Human" && string(word)!="human")
		{

			delete player;

			ai=true;

			if (string(word)=="Aggressive" || string(word)=="aggressive")
				aiPlayer->setStrategy(new AggressiveStrat);
			else if (string(word)=="Defensive" || string(word)=="defensive")
				aiPlayer->setStrategy(new DefensiveStrat);
			else if (string(word)=="Random" || string(word)=="random")
				aiPlayer->setStrategy(new RandomStrat);
			else 
			{
				throw CorruptedSaveFile(CorruptedSaveFile::STRATEGY);
			}

		}
		else 
			delete aiPlayer;




		file.getline(word,256);

		string addedCountries=word;

		string delimiter = ",";
		size_t pos = 0;
		string token;

		if (ai==false)
			game->addPlayer(player);
		else
			game->addPlayer(aiPlayer);



		while ((pos = addedCountries.find(delimiter)) != string::npos) {
			token = addedCountries.substr(0, pos);
			Country* country= map->getCountry(token);
			if (ai==false)
			{
				player->addCountry(country);
				country->setPlayer(player);
				game->getPlayerViewVector()[player->getPlayerID()-1]->attachCountryToPlayerView(country);
			}
			else
			{
				aiPlayer->addCountry(country);
				country->setPlayer(aiPlayer);
				game->getPlayerViewVector()[aiPlayer->getPlayerID()-1]->attachCountryToPlayerView(country);
			}

			addedCountries.erase(0, pos + delimiter.length());
		}


		file.getline(word,256,',');


	}

	file.getline(word,256);

	file.getline(word,256,',');

	while (string(word)!="Current Turn")
	{


		int id=atoi(word);


		file.getline(word,256);

		string addedCards=word;

		string delimiter = ",";
		size_t pos = 0;
		string token;

		while ((pos = addedCards.find(delimiter)) != string::npos) {

			Player::Card card;

			string cardString = addedCards.substr(0, pos);

			if (string(cardString)=="Infantry" || string(cardString)=="infantry")
				card=Player::INFANTRY;
			else if (string(cardString)=="Cavalry" || string(cardString)=="cavalry")
				card=Player::CAVALRY;
			else if (string(cardString)=="Artillery" || string(cardString)=="artillery")
				card=Player::ARTILLERY;
			else if (string(cardString)=="Redemptions" || string(cardString)=="redemptions")
			{
				addedCards.erase(0, pos + delimiter.length());
				int num=stoi( addedCards.substr(0, pos));
				for (int i=0; i<num;i++)
					game->getPlayerVector()[id-1]->addRedemption();
				break;
			}
			else
			{
				throw CorruptedSaveFile(CorruptedSaveFile::CARD);
			}

			addedCards.erase(0, pos + delimiter.length());

			int num=stoi( addedCards.substr(0, pos));

			for (int i=0; i<num;i++)
				game->getPlayerVector()[id-1]->addCard(card);

			pos = addedCards.find(delimiter);

			addedCards.erase(0, pos + delimiter.length());

		}




		file.getline(word,256,',');


	}

}


void GameBuilder::startGameFromTurn(){

	ifstream file(fileName);

	char word[256];

	while (string(word)!="Current Turn,")
		file.getline(word,256);

	file.getline(word,256,',');

	string phase=word;

	file.getline(word,256,',');

	int id=atoi(word);

	game->startGame(id, phase);

}

void GameBuilder::attachMapToGame(){

	game->setMap(map);

}

Game* GameBuilder::getGame(){
	return game;
}

Map * GameBuilder::getMap(){
	return map;
}

GameBuilder::~GameBuilder(void)
{
}
