
#include "stdafx.h"
#include "ConsoleUI.h"
#include <iostream>
#include <algorithm>

using namespace std;
const char* ConsoleUI::_genericConsoleTitle="Risk";

ConsoleUI::ConsoleUI(void)
{
	_console = new CConsoleLoggerEx();
}

void ConsoleUI::displaySeparatorLine(){

	for (int i=0; i < secondColumnDisplayPosition*1.5 ; i++)
		_console->printf("-");
	_console->printf("\n");
}

void ConsoleUI::printSeparatorLine(){

	for (int i=0; i < secondColumnDisplayPosition*1.5 ; i++)
		cout <<("-");
	cout <<("\n");
}

void ConsoleUI::printLongSeparatorLine(){

	for (int i=0; i < endOfDisplayPosition ; i++)
		cout <<("-");
	cout <<("\n");
}

void ConsoleUI::display(){

	_console->Create(_genericConsoleTitle);
	displaySubjectData();
}


void ConsoleUI::display(std::string title){

	_console->Create(title.c_str());
	displaySubjectData();
}

void ConsoleUI::closeDisplay(){
	_console->Close();
}

bool ConsoleUI::yesOrNoQuery(std::string message){

	cout << message;

	cout << " Y/N" << endl;

	cin.clear();

	string input;

	cin >> input;

	transform(input.begin(), input.end(), input.begin(), ::tolower);

	while (string(input)!="no" && string(input)!="yes" && string(input)!="n" && string(input)!="y")
	{
		cout << "You have entered an invalid input. Please enter yes or no." << endl;

		std::cout << message << " Y/N " << std::endl;

		cin.clear();

		cin.ignore(numeric_limits<streamsize>::max(), '\n');

		cin >> input;

	}

	if ( string(input)=="yes"  || string(input)=="y")
		return true;
	else return false;

}

ConsoleUI::~ConsoleUI(void)
{
}
