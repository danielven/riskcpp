#pragma once
#include "Observer.h"
#include "ConsoleUI.h"
#include "Game.h"

class GameView : public Observer, public ConsoleUI
{
public:
	GameView(Game*);
	void Update();
	void displayStartOfPhase(string,Player* const);
	int redeemCardsMenu(Player*, bool *);
	int addArmyToCountryMenu(Country*, int, string);
	int selectMovingCountries(Country*,Country*, string);
	int queryForHumanPlayers(int);
	int queryForAIPlayers(int,int,int);
	bool queryUserForContinuingAttack(Player*);
	void printEndOfGameMessage(Player*);
	Country* reinfocePlayerMenu(Player*,int, string);
	Country* selectAttackingCountryMenu(Player* , vector<Country*>);		//For asking the user where to attack from.
	Country* selectDefendingCountryMenu(Player* , vector<Country*> );		//For asking the user where to attack.
	Country* selectFortifyingCountryMenu(Player*, vector<Country*>);		//For asking the user where to fortify from
	Country* selectCountryToFortifyMenu(Player*, vector<Country*>);		//For asking the user where to fortify.
	string queryUserForFileName();


	~GameView(void);
private:
	Game* _gameObserved;

	void checkForMenuSelection(string, string, Player*);
	Country* queryUserForCountry(vector<Country*>,string,string);
	void displaySubjectData();
};

