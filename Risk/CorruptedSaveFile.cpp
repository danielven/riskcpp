#include "stdafx.h"
#include "CorruptedSaveFile.h"

CorruptedSaveFile::CorruptedSaveFile(Type t) {
	CorruptedSaveFile::type = t;
}

void CorruptedSaveFile::handle() {
	if (type == CARD) {
		cout << "Corrupted save file, check cards for players" << endl;
		cout << "Ending game." << endl;
		system("PAUSE");
		exit(0);
	}
	if (type == PHASE) {
		cout << "Corrupted save file, check game phases" << endl;
		cout << "Ending game." << endl;
		system("PAUSE");
		exit(0);
	}
	if (type == STRATEGY) {
		cout << "Save file is corrupted. Check strategy for AI players." << endl;
		cout << "Ending game." << endl;
		system("PAUSE");
		exit(0);
	}
	if (type == SAVE) {
		cout << "Cannot save the map, file possibly not accessible." << endl;
		cout << "Ending game." << endl;
		system("PAUSE");
		exit(0);
	}
}

CorruptedSaveFile::~CorruptedSaveFile(void)
{
}
