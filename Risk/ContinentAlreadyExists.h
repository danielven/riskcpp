#pragma once
#include <exception>
using namespace std;

class ContinentAlreadyExists : public exception
{
public:
	ContinentAlreadyExists(void);
	const char* what();
	~ContinentAlreadyExists(void);
};

