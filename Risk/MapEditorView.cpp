#include "stdafx.h"
#include "MapEditorView.h"
#include <sstream>
#include <algorithm>



MapEditorView::MapEditorView(MapEditor* mapEditor)
{

	_subject=mapEditor;
	_map=mapEditor->getMap();
	mapEditor->getMap()->Attach(this);

}

void MapEditorView::displaySubjectData(){

	displaySeparatorLine();
	_console->printf("MAP\n");
	vector<Continent*> continents = _map->getContinentVector();
	for (vector<Continent*>::iterator it = continents.begin(); it != continents.end(); ++it) {
		_console->printf("* ");
		_console->printf(((*it)->getName()).c_str());
		int nameSize = ((*it)->getName().size());
		for (nameSize; nameSize<ConsoleUI::secondColumnDisplayPosition; nameSize++)
			_console->printf(" ");

		_console->printf("%d",(*it)->getBonus());

		_console->printf(" bonus armies.\n");

		_console->printf("Contained countries:\n");

		vector<Country*> _countries = (*it)->getCountries();

		for (vector<Country*>::iterator it2=_countries.begin(); it2!=_countries.end(); it2++)
		{
			_console->printf((*it2)->getName().c_str());
			_console->printf(" ");
		}
		_console->printf("\n");
		displaySeparatorLine();
	}

	vector<Country*> _countries = _map->getCountryVector();

	displaySeparatorLine();

	_console->printf("ADJACENCIES\n");

	for (vector<Country*>::iterator country_it = _countries.begin() ; country_it != _countries.end(); ++country_it){

		stringstream output;

		output << " *"<< (*country_it)->getName() << ": ";
		vector<Country*> paths=(*country_it)->getPaths();

		for (vector<Country*>::iterator adjac_it = paths.begin() ; adjac_it != paths.end(); ++adjac_it){
			output << (*adjac_it)->getName() << " ";
		}
		output << endl;

		_console->printf(output.str().c_str());
	}

	displaySeparatorLine();

	_console->printf("MENU\n");
	_console->printf("1. Add a country\n");
	_console->printf("%d",options);
	_console->printf(". Exit");
}

int MapEditorView::promptUserForInput(){

	cout << "Please enter one of the options available in the editor menu (integer from 1 to " << options << ":" << endl;

	int selection;

	cin.clear();

	cin.ignore();

	cin >> selection;

	while (selection <= 0 || selection > options || cin.fail())
	{

		cout << "Not a valid menu option." << endl;

		cin.clear();

		cin.ignore(numeric_limits<streamsize>::max(), '\n'); 

		cin >> selection;

	}

	return selection;

}

pair<Country*,Continent*> MapEditorView::buildCountry(){

	cout << "Enter the name of the new country:" << endl;

	string selection;

	cin.clear(); 

	cin >> selection;

	while (cin.fail() || selection.size()==0)
	{

		cout << "Not a valid option." << endl;

		cin.clear();

		cin.ignore(numeric_limits<streamsize>::max(), '\n'); 

		cin >> selection;

	}

	Country* country = new Country(selection);

	cout << "Enter containing continent:" << endl;

	cin.clear();

	cin.ignore(numeric_limits<streamsize>::max(), '\n'); 

	cin >> selection;

	Continent* continent = _map->getContinent(selection);

	while (cin.fail() || continent==nullptr)
	{

		cout << "Not a continent in the current map, review the options on the viewer." << endl;

		cin.clear();

		cin.ignore(numeric_limits<streamsize>::max(), '\n'); 

		cin >> selection;

		continent = _map->getContinent(selection);

	}

	pair<Country*,Continent*> pair (country,continent);

	return pair;
}



void MapEditorView::Update(){

	_console->cls();

	displaySubjectData();

}

MapEditorView::~MapEditorView()
{
}
