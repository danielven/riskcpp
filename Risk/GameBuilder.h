#pragma once
#include "Map.h"
#include "Game.h"
class GameBuilder
{
		friend class Game;

public:
	GameBuilder(void);
	void createNewGame(string);
	void buildMap();
	void buildPlayers();
	void startGameFromTurn();
	void attachMapToGame();
	Game * getGame();
	Map * getMap();
	~GameBuilder(void);

protected:
	Game * game;
	Map * map;
	string fileName;
};

