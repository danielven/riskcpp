#include "stdafx.h"
#include "AggressiveStrat.h"


using namespace std;

const string AggressiveStrat::className="Aggressive";

AggressiveStrat::AggressiveStrat()
{
}

//Player attacks countries with less armies.
void AggressiveStrat::execute(ComputerPlayer* player)
{
	map<Country*, set<Country*>> strongCountryMap=player->getStrongCountryMap();

	if (strongCountryMap.empty())
	{
		cout << "Aggressive player " << player->getPlayerID() << " (" << player->getName() << ") abstains." << endl;
		return;
	}

	if(!strongCountryMap.empty())
	for (map<Country*, set<Country*>>::iterator it=strongCountryMap.begin(); it!=strongCountryMap.end(); it++)
	{

		
		Country* strongCountry=it->first;

		for (set<Country*>::iterator it2=it->second.begin(); it2!= it->second.end(); it2++)
		{

			while(it->first->getArmies()> (*it2)->getArmies() && it->first->getPlayer() != (*it2)->getPlayer())
			{Battle battle(it->first, *it2);

			battle.doBattle(false);
			}
		}
	}

			cout << "Attacking player " << player->getPlayerID() << " (" << player->getName() << ") stops attacking." << endl <<endl;


}


AggressiveStrat::~AggressiveStrat()
{
}
