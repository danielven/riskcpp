Continents
North America,5
South America,2
Africa,3
Europe,5
Asia,7
Australia,2
Countries,
Alaska,70,126,North America,Northwest Territory,Alberta,Kamchatka,5
Northwest Territory,148,127,North America,Alaska,Alberta,Ontario,Greenland,3
Alberta,147,163,North America,Alaska,Northwest Territory,Ontario,Western United States,7
Greenland,324,76,North America,Northwest Territory,Ontario,Quebec,Iceland,5
Ontario,199,167,North America,Northwest Territory,Alberta,Greenland,Quebec,Western United States,Eastern United States,5
Quebec,253,166,North America,Greenland,Ontario,Eastern United States,8
Western United States,159,204,North America,Alberta,Ontario,Eastern United States,Central America,4
Eastern United States,219,216,North America,Ontario,Quebec,Western United States,Central America,6
Central America,183,262,North America,Western United States,Eastern United States,Venezuala,6
Venezuala,259,303,South America,Central America,Peru,Brazil,4
Peru,262,349,South America,Venezuala,Brazil,Argentina,5
Argentina,263,407,South America,Peru,Brazil,5
Brazil,308,337,South America,Venezuala,Peru,Argentina,North Africa,4
North Africa,420,264,Africa,Brazil,Western Europe,Southern Europe,Egypt,East Africa,Congo,4
Congo,475,318,Africa,North Africa,East Africa,South Africa,5
South Africa,483,371,Africa,Congo,East Africa,Madagascar,5
Madagascar,536,361,Africa,South Africa,East Africa,2
East Africa,517,298,Africa,North Africa,Congo,South Africa,Madagascar,Egypt,Middle East,6
Egypt,480,249,Africa,North Africa,East Africa,Southern Europe,Middle East,5
Iceland,380,126,Europe,Greenland,Great Britain,Scandinavia,5
Great Britain,419,169,Europe,Iceland,Scandinavia,Western Europe,Northern Europe,6
Western Europe,423,202,Europe,North Africa,Great Britain,Northern Europe,Southern Europe,4
Scandinavia,463,127,Europe,Iceland,Great Britain,Northern Europe,Ukraine,5
Northern Europe,460,173,Europe,Great Britain,Western Europe,Scandinavia,Ukraine,Southern Europe,7
Southern Europe,473,197,Europe,North Africa,Egypt,Western Europe,Northern Europe,Ukraine,Middle East,6
Ukraine,524,157,Europe,Scandinavia,Northern Europe,Southern Europe,Ural,Afghanistan,Middle East,6
Middle East,530,234,Asia,East Africa,Egypt,Southern Europe,Ukraine,Afghanistan,India,3
Ural,613,136,Asia,Ukraine,Siberia,China,Afghanistan,3
Afghanistan,585,192,Asia,Ukraine,Middle East,Ural,China,India,6
India,612,249,Asia,Middle East,Afghanistan,China,Siam,3
Siberia,666,110,Asia,Ural,Yatusk,Irkutsk,Mongolia,China,4
China,662,217,Asia,Ural,Afghanistan,India,Siberia,Mongolia,Siam,8
Siam,671,270,Asia,India,China,Indonesia,4
Mongolia,707,188,Asia,Siberia,China,Irkutsk,Japan,Kamchatka,5
Irkutsk,698,152,Asia,Siberia,Mongolia,Yatusk,Kamchatka,6
Yatusk,738,118,Asia,Siberia,Irkutsk,Kamchatka,5
Kamchatka,806,125,Asia,Alaska,Mongolia,Irkutsk,Yatusk,Japan,4
Japan,759,220,Asia,Mongolia,Kamchatka,5
Indonesia,698,314,Australia,Siam,New Guinea,Western Australia,5
New Guinea,768,325,Australia,Indonesia,Western Australia,Eastern Australia,5
Western Australia,729,373,Australia,Indonesia,New Guinea,Eastern Australia,5
Eastern Australia,779,381,Australia,New Guinea,Western Australia,4
Players,
JEsus,human,North Africa,Alberta,Mongolia,Japan,Alaska,Iceland,Ontario,Yatusk,Great Britain,
Aggressive,Aggressive,China,Peru,Central America,Middle East,Congo,Southern Europe,Western Australia,New Guinea,Madagascar,
Defensive,Defensive,Quebec,Siam,East Africa,South Africa,Ural,Venezuala,Ukraine,Brazil,
Aggressive,Aggressive,Afghanistan,Northern Europe,Eastern United States,Northwest Territory,Argentina,Siberia,Indonesia,India,
Aggressive,Aggressive,Egypt,Irkutsk,Kamchatka,Scandinavia,Eastern Australia,Greenland,Western Europe,Western United States,
Cards,
1,Infantry,0,Cavalry,0,Artillery,0,Redemptions,0
2,Infantry,0,Cavalry,0,Artillery,0,Redemptions,0
3,Infantry,0,Cavalry,0,Artillery,0,Redemptions,0
4,Infantry,0,Cavalry,0,Artillery,0,Redemptions,0
5,Infantry,0,Cavalry,0,Artillery,0,Redemptions,0
Current Turn,
fortification,1