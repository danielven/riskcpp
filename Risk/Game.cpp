/*
* Game.cpp
*
*/

#include "stdafx.h"

#include "Game.h"
#include <iostream>
#include "Player.h"
#include "Country.h"
#include "CorruptedSaveFile.h"
#include <algorithm>
#include <sstream>
#include <time.h>
using namespace std;

Game::Game() {

	num_of_players=0;

	num_of_AI_players=0;

	max_num_countries = 42;   //Generic map only.	

	view = new GameView(this);

	srand ( (unsigned int)time(NULL) );

}

Game::Game(Map *map) {

	num_of_players=0;

	num_of_AI_players=0;

	max_num_countries = map->getCountryVector().size();  

	view = new GameView(this);


	srand ((unsigned int)time(NULL));

	_map = map;

}


//Starts the game while prompting the user for human player names.
void Game::initializePlayers(){

	num_of_players=view->queryForHumanPlayers(max_players);

	int minNumber=0;

	if (num_of_players==1)
		minNumber=1;

	num_of_AI_players= view->queryForAIPlayers(minNumber,max_players,num_of_players);

	for (int i=0; i<Game::num_of_players; i++){

		cout << "What's player? " << i + 1 <<"'s name?"<<endl;

		string tempName;

		cin >> tempName;

		playerVector.emplace_back(new HumanPlayer(tempName, i+1));

		playerViews.emplace_back(new PlayerView(playerVector[i]));

		playerVector[i]->Attach(playerViews[i]);

	}	

	int goal=num_of_players;

	for (int i=num_of_players; i<num_of_AI_players+goal; i++)
	{

		string name;

		int randNum = rand() % 3;

		if (randNum==0)
		{
			name = AggressiveStrat::className;
			playerVector.emplace_back(new ComputerPlayer(name,i+1,new AggressiveStrat()));
		}
		else if (randNum==1)
		{

			name = DefensiveStrat::className;

			playerVector.emplace_back(new ComputerPlayer(name,i+1,new DefensiveStrat()));

		}
		else
		{
			name =  RandomStrat::className;

			playerVector.emplace_back(new ComputerPlayer(name,i+1,new RandomStrat()));
		}

		playerViews.emplace_back(new PlayerView(playerVector[i]));

		playerVector[i]->Attach(playerViews[i]);

		num_of_players++;

	}



	randomCountryAssign(_map);

	_stats= new BasicStats(playerVector);

	_stats->display();

}

//Randomly assigns armies to all players in the game. Each country gives the player access to 3 armies. In the future different countries will change the amount of armies the player gets.
void Game::randomlyAssignArmies(){

	for (int i=0; i< num_of_players; i++)
	{
		//For now we will assume each initial country gives the player 3 armies to spread out. Each country has at least 2 armies.
		int totalArmies=3*(getPlayerVector()[i]->getCountriesOwned());

		int j=0;

		vector<Country*> countries= getPlayerVector()[i]->getCountryVector();

		for (vector<Country*>:: iterator it2= countries.begin(); it2!=countries.end(); ++it2)
		{
			(*it2)->addArmy(2);
		}

		vector<Country*>::iterator it= countries.begin();

		while (j<totalArmies)
		{

			if (it==countries.end())
				it= countries.begin();


			int add= rand() % 4;

			j+=add;

			if (j>totalArmies)
			{
				add=j-totalArmies;

				j=totalArmies;

			}

			(*it)->addArmy(add);

			it++;

		}

	}
}




//Utilities for shuffling vectors.
void Game::randomlyShuffle(vector<Country*>* vector)
{
	std::random_shuffle(vector->begin(), vector->end());

}




//Randomly assigns all the countries in the map to all players, as evenly as possible.
void Game::randomCountryAssign(Map* map){

	vector<Country*> countryVector = map->getCountryVector();

	std::random_shuffle(countryVector.begin(), countryVector.end());

	int index=0;

	for (int  i=0; i <= (int)countryVector.size()-1; i++)

	{
		countryVector[i]->setPlayer(playerVector[index]);

		playerViews[index]->attachCountryToPlayerView(countryVector[i]);

		if (index==num_of_players-1)
			index=0;
		else index++;

	}


}

void Game::reinforcementPhase(HumanPlayer* player){

	view->displayStartOfPhase("Reinforcement",player);

	int addedArmies=calculateReinforcingArmiesForPlayer(player);

	reinforcePlayer(addedArmies+ redeemCards(player), player);

}

void Game::reinforcementPhase(ComputerPlayer* player){


	view->displayStartOfPhase("Reinforcement",player);

	int addedArmies=calculateReinforcingArmiesForPlayer(player);

	reinforcePlayer(addedArmies+ redeemCards(player), player);

}

void Game::reinforcePlayer(int addedArmies, HumanPlayer* player){

	while(addedArmies>0)
	{

		Country* addedCountry= view->reinfocePlayerMenu(player, addedArmies,"reinforcement");

		int armyInput=1;

		if (addedArmies>1)
			armyInput = view->addArmyToCountryMenu(addedCountry, addedArmies,"reinforcement");

		addedCountry->addArmy(armyInput);

		addedArmies-=armyInput;
	}
}

void Game::reinforcePlayer(int addedArmies, ComputerPlayer* player){

	while(addedArmies>0)
	{

		int randIndex =  rand() % player->getCountryVector().size();

		int randArmies= rand() % addedArmies + 1;

		player->getCountryVector()[randIndex]->addArmy(randArmies);

		addedArmies-=randArmies;
	}


}

//When a main phase beggins, the player obtains a certain number of armies te distribute as he/she sees fit. Since this is calculated solely with attributs of the player, this method will be in this Player class and not the Game class.
//These calcutalions happen according to standard Risk rules, without Risk "cards" (see http://en.wikipedia.org/wiki/Risk_%28game%29#Player_turn).
int Game::calculateReinforcingArmiesForPlayer(Player* player){

	int result=player->getCountriesOwned()/3;
	if (result<3)
		result=3;
	//Bonus armies according to Continents conquered.

	set<Continent*> continentSet = player->getContinentSet();

	for (set<Continent*>::iterator it=continentSet.begin(); it!= continentSet.end(); it++)
		result += (*it)->getBonus();

	return result;

}

void Game::attackPhase(HumanPlayer* player){

	view->displayStartOfPhase("Attack",player);

	vector<Country*> availableCountriesForAttack = player->getCountryVector();

	bool attacking=true;

	while(attacking==true)
	{

		//Eliminate countries where all adjacent countries are owned by this player, or the number of armies is 1.
		vector<Country*>::iterator it = availableCountriesForAttack.begin();

		while (it!=availableCountriesForAttack.end())
		{

			vector<Country*> tempVector2 = (*it)->getAdjacentEnemyCountries(player);

			if (tempVector2.empty() || (*it)->getArmies()<=1)
			{
				if (it==prev(availableCountriesForAttack.end()))
				{
					availableCountriesForAttack.erase(remove(availableCountriesForAttack.begin(),availableCountriesForAttack.end(),(*it)),availableCountriesForAttack.end());
					break;
				}
				else
					availableCountriesForAttack.erase(remove(availableCountriesForAttack.begin(),availableCountriesForAttack.end(),(*it)),availableCountriesForAttack.end());
			}
			else
				it++;

		}

		Country* attackingCountry = view->selectAttackingCountryMenu(player, availableCountriesForAttack);

		if (attackingCountry==nullptr)
			return;

		else
		{
			vector<Country*> availableCountriesToAttack= attackingCountry->getAdjacentEnemyCountries(player);

			Country* defendingCountry = view->selectDefendingCountryMenu(player, availableCountriesToAttack);

			Player* defendingPlayer = defendingCountry->getPlayer();

			if (defendingCountry==nullptr)
				break;
			else
			{

				playerViews[defendingPlayer->getPlayerID()-1]->display();


				Battle battle(attackingCountry,defendingCountry);

				bool conquer = battle.doBattle(false);

				if (conquer==true)
				{
					playerViews[player->getPlayerID()-1]->attachCountryToPlayerView(attackingCountry);

					playerViews[defendingPlayer->getPlayerID()-1]->detatchCountryFromPlayerView(defendingCountry);
				}

				playerViews[defendingPlayer->getPlayerID()-1]->closeDisplay();

				if (defendingPlayer->getCountryVector().empty())
					removePlayerFromGame(defendingPlayer);
			}
		}

		if (!victoryCondition())
			attacking= view->queryUserForContinuingAttack(player);

	}
}


void Game::attackPhase(ComputerPlayer* player){

	view->displayStartOfPhase("Attack",player);

	player->executeStrategy();	

	for (int i=0; i<=((int)playerVector.size())-1; i++)
		if (playerVector[i]->getCountryVector().empty())
			removePlayerFromGame(playerVector[i]);

}

void Game::fortificationPhase(HumanPlayer* player){

	view->displayStartOfPhase("Fortification",player);

	vector<Country*> availableCountriesToFortifyFrom = player->getCountryVector();

	//Eliminate countries with no adjacent countries owned by this player, or the number of armies is 1.
	vector<Country*>::iterator it = availableCountriesToFortifyFrom.begin();

	while (it!=availableCountriesToFortifyFrom.end())
	{

		vector<Country*> tempVector2 = (*it)->getAdjacentAlliedCountries(player);

		if (tempVector2.empty() || (*it)->getArmies()<=1)
		{
			if (it==prev(availableCountriesToFortifyFrom.end()))
			{
				availableCountriesToFortifyFrom.erase(remove(availableCountriesToFortifyFrom.begin(),availableCountriesToFortifyFrom.end(),(*it)),availableCountriesToFortifyFrom.end());
				break;
			}
			else
				availableCountriesToFortifyFrom.erase(remove(availableCountriesToFortifyFrom.begin(),availableCountriesToFortifyFrom.end(),(*it)),availableCountriesToFortifyFrom.end());
		}
		else
			it++;

	}


	Country* fortifyingCountry = view->selectFortifyingCountryMenu(player, availableCountriesToFortifyFrom);

	if (fortifyingCountry==nullptr)
	{
		if (availableCountriesToFortifyFrom.empty())
			return;
		else
			fortificationPhase(player);
	}

	Country* fortifiedCountry = view->selectCountryToFortifyMenu(player, fortifyingCountry->getAdjacentAlliedCountries(player));

	//Error handling, should never happen.
	if (availableCountriesToFortifyFrom.empty())
	{fortificationPhase(player);
	return;
	}

	if (fortifiedCountry==nullptr)
	{
		fortificationPhase(player);
		return;
	}

	int movingArmies = view->selectMovingCountries(fortifyingCountry,fortifiedCountry,"attack");

	if (movingArmies==0)
	{
		fortificationPhase(player);
		return;
	}

	fortifyingCountry->substractArmy(movingArmies);

	fortifiedCountry->addArmy(movingArmies);

}

void Game::fortificationPhase(ComputerPlayer* player){

	view->displayStartOfPhase("Fortification",player);

	vector<Country*> availableCountriesToFortifyFrom = player->getCountryVector();

	//Eliminate countries with no adjacent countries owned by this player, or the number of armies is 1.
	vector<Country*>::iterator it = availableCountriesToFortifyFrom.begin();

	while (it!=availableCountriesToFortifyFrom.end())
	{

		vector<Country*> tempVector2 = (*it)->getAdjacentAlliedCountries(player);

		if (tempVector2.empty() || (*it)->getArmies()<=1)
		{
			if (it==prev(availableCountriesToFortifyFrom.end()))
			{
				availableCountriesToFortifyFrom.erase(remove(availableCountriesToFortifyFrom.begin(),availableCountriesToFortifyFrom.end(),(*it)),availableCountriesToFortifyFrom.end());
				break;
			}
			else
				availableCountriesToFortifyFrom.erase(remove(availableCountriesToFortifyFrom.begin(),availableCountriesToFortifyFrom.end(),(*it)),availableCountriesToFortifyFrom.end());
		}
		else
			it++;

	}

	if (availableCountriesToFortifyFrom.size()==0)
		return;

	int randIndex = rand() % availableCountriesToFortifyFrom.size();

	Country* fortifyingCountry = availableCountriesToFortifyFrom[randIndex];

	if (fortifyingCountry==nullptr)
	{
		if (availableCountriesToFortifyFrom.empty())
			return;
		else
			fortificationPhase(player);
	}

	randIndex = rand() % fortifyingCountry->getAdjacentAlliedCountries(player).size();

	Country* fortifiedCountry= fortifyingCountry->getAdjacentAlliedCountries(player)[randIndex];

	int movingArmies = rand() % (fortifyingCountry->getArmies()-1) + 1;

	fortifyingCountry->substractArmy(movingArmies);

	fortifiedCountry->addArmy(movingArmies);



}


bool Game::victoryCondition(){

	if (!(playerVector.size()>1))
		return true;

	bool cond=false;

	for (int i=0; i<Game::num_of_players-1; i++){


		if (playerVector[i]->getCountriesOwned() == max_num_countries) 
			cond = true;
	}

	return cond;
}

//Overloaded method for evaluating victory condition to a specific player
bool Game::victoryCondition(Player * player){

	if (!(playerVector.size()>1))
		return true;

	return player->getCountriesOwned() == max_num_countries;
}


vector<Player*> Game::getPlayerVector(){

	return playerVector;
}

int Game::getNumOfPlayers(){

	return num_of_players;
};

void Game:: printGameState(){

	for (int i = 0; i < num_of_players ; i++){

		cout << "\nPlayer " << playerVector[i]->getPlayerID() << " (" << playerVector[i]->getName() << ") has " << playerVector[i]->getCountriesOwned() << " countrie(s) and " << playerVector[i]->getTotalNumberOfCards() << " card(s)." << endl;

		//		playerVector[i]->printCountries();

		//	playerVector[i]->printCards();

		cout << endl;

	}
}


Player* Game::winningPlayer(){
	if (victoryCondition() == false)
		cout << "Error, no winning player yet." << endl;  //needs proper error handling
	else
	{
		if (num_of_players==1 && playerVector.size()==1)
			return playerVector.front();

		for (int i = 0; i<num_of_players-1; i++){
			if (playerVector[i]->getCountriesOwned() == max_num_countries) return playerVector[i];
		}
	}

	return nullptr;

}

void Game::addPlayer(HumanPlayer* newPlayer){

	playerVector.emplace_back(newPlayer);

	playerViews.emplace_back(new PlayerView(newPlayer));

	newPlayer->Attach(playerViews[newPlayer->getPlayerID()-1]);

	num_of_players++;
}

void Game::addPlayer(ComputerPlayer * newAIplayer){

	playerVector.emplace_back(newAIplayer);

	playerViews.emplace_back(new PlayerView(newAIplayer));

	newAIplayer->Attach(playerViews[newAIplayer->getPlayerID()-1]);

	num_of_AI_players++;

	num_of_players++;

}

//Bagins the round robin loop for the game.
void Game::startGame(){


	int i=0;

	bool end=false;

	while (end==false)
	{
		playerViews[i]->display();

		startPhaseForPlayer(playerVector[i],"reinforcement");

		startPhaseForPlayer(playerVector[i],"attack");

		startPhaseForPlayer(playerVector[i],"fortification");

		if (victoryCondition(playerVector[i]))
			end=true;

		playerViews[i]->closeDisplay();

		if (i==num_of_players-1)
			i=0;
		else
			i++;


	}

	if (end)
		view->printEndOfGameMessage(winningPlayer());
}

//Inputs are id of whoever's turn it is, and what phase it is. Valid inputs for the phase are reinforcement, attack and fortification.
void Game::startGame(int id, string phase){

	Player* player= playerVector[id-1];

	transform(phase.begin(), phase.end(), phase.begin(), ::tolower);

	_stats= new BasicStats(playerVector);

	_stats->display();

	playerViews[player->getPlayerID()-1]->display();

	transform(phase.begin(), phase.end(), phase.begin(), ::tolower);

	if (phase=="reinforcement")
	{
		startPhaseForPlayer(player,phase);

		startPhaseForPlayer(player,"attack");

		startPhaseForPlayer(player,"fortification");
	}
	else if (phase=="attack")
	{
		startPhaseForPlayer(player,phase);

		startPhaseForPlayer(player,"fortification");
	}
	else if (phase=="fortification")
		startPhaseForPlayer(player,phase);
	else
	{
		throw CorruptedSaveFile(CorruptedSaveFile::PHASE);
	}

	playerViews[player->getPlayerID()-1]->closeDisplay();

	bool end=victoryCondition(player);

	int i = player->getPlayerID();


	while (end==false)
	{


		playerViews[i]->display();


		startPhaseForPlayer(playerVector[i],"reinforcement");

		startPhaseForPlayer(playerVector[i],"attack");

		startPhaseForPlayer(playerVector[i],"fortification");

		playerViews[i]->closeDisplay();

		if (victoryCondition(playerVector[i]))
			end=true;

		if (i==num_of_players-1)
			i=0;
		else
			i++;


	}

	if (end)
		view->printEndOfGameMessage(winningPlayer());
}


int Game::redeemCards(Player* player){

	int i=0;

	int j=0;

	int z=0;

	bool hasAnyOption=false; //becomes true if any option is availbable

	bool* options= new bool[Player::numberOfCardTypes];
	for (z; z<Player::numberOfCardTypes-1; z++)
		options[z] = false;

	options[Player::numberOfCardTypes]=true; // this indicates one of every type

	for (i; i<= Player::numberOfCardTypes-1; i++)
	{

		if (player->getCards()[i]>=3)
		{

			options[i]=true;

			hasAnyOption=true;

		}

		if (player->getCards()[i]<=0)
			options[Player::numberOfCardTypes]=false;
	}

	bool * optionArray = options;

	int result=0;

	if (hasAnyOption || options[Player::numberOfCardTypes])
	{
		int input =view->redeemCardsMenu(player, options);
		if (input==Player::numberOfCardTypes+1)
			return 0;
		else if (input==Player::numberOfCardTypes)
		{
			player->substractCard(0);
			player->substractCard(1);
			player->substractCard(2);
		}
		else 
			for (int i=0; i<Player::numberOfCardTypes; i++)
				player->substractCard(input);

		player->addRedemption();
		return (player->getRedemptions()*5);
	}

	return 0;
}


int Game::redeemCards(ComputerPlayer* player){

	int i=0;

	int z=0;

	bool hasAnyOption=false; //becomes true if any option is availbable

	bool* options= new bool[Player::numberOfCardTypes];
	for (z; z<Player::numberOfCardTypes-1; z++)
		options[z] = false;

	z++;
	options[z]=true;

	for (i; i<Player::numberOfCardTypes-1; i++)
	{

		if (player->getCards()[i]>=3)
		{

			options[i]=true;

			hasAnyOption=true;

		}

		if (player->getCards()[i]<=0)
			options[Player::numberOfCardTypes]=false;
	}

	int input=Player::numberOfCardTypes+1;

	for (int j=0; j <Player::numberOfCardTypes+1; j++)
	{
		if (options[j]==true)
			input=j;
	}

	if (input==Player::numberOfCardTypes+1)
		return 0;
	else if (input==Player::numberOfCardTypes)
	{
		player->substractCard(0);
		player->substractCard(1);
		player->substractCard(2);
	}
	else 
		for (int i=0; i<Player::numberOfCardTypes; i++)
			player->substractCard(input);

	player->addRedemption();
	return (player->getRedemptions()*5);

	return 0;
}

void Game::setMap(Map* map){
	_map=map;
	max_num_countries=map->getCountryVector().size();
}

vector<PlayerView*> Game::getPlayerViewVector(){
	return playerViews;
}

void Game::saveGame(Map* map, string fileName, string phase, Player* currentPlayer){

	ofstream saveFile (fileName);
	if (saveFile.is_open())
	{
		saveFile << "Continents" << '\n';// start to save continent
		for(size_t i = 0; i< map->getContinentVector().size();i++)
		{
			saveFile << map->getContinentVector()[i]->getName() << "," << map->getContinentVector()[i]->getBonus() << '\n';
		}// finish saving the continent


		saveFile << "Countries," << '\n';// start to save countries
		for(size_t i = 0; i< map->getCountryVector().size();i++)
		{
			saveFile << map->getCountryVector()[i]->getName() << "," << map->getCountryVector()[i]->getCoords().first << "," << map->getCountryVector()[i]->getCoords().second << "," <<  map->getCountryVector()[i]->getContinent()->getName()
				<< ",";
			for(size_t j = 0; j < map->getCountryVector()[i]->getAdjacentCountries().size();j++)
			{
				saveFile << map->getCountryVector()[i]->getAdjacentCountries()[j]->getName() << ",";
			}

			saveFile << map->getCountryVector()[i]->getArmies() << '\n';
		}//finish saving the countries

		saveFile << "Players," << '\n';//start to save the player
		for(size_t i = 0; i < playerVector.size() - num_of_AI_players;i++)
		{
			saveFile << playerVector[i]->getName() << ",human,";
			for(size_t j = 0; j < playerVector[i]->getCountryVector().size();j++)
			{
				saveFile << playerVector[i]->getCountryVector()[j]->getName() << ",";
			}
			saveFile << "\n";
		}//finish saving the human player

		for(size_t i = playerVector.size() - num_of_AI_players; i < playerVector.size();i++)
		{
			saveFile << playerVector[i]->getName() << "," << dynamic_cast<ComputerPlayer*>(playerVector[i])->getStrategy()->getStrategyName() << ",";
			for(size_t j = 0; j < playerVector[i]->getCountryVector().size();j++)
			{
				saveFile << playerVector[i]->getCountryVector()[j]->getName() << ",";
			}
			saveFile << "\n";
		}//finish saving the AI player

		saveFile << "Cards," << '\n';// start to save cards
		for(size_t i = 0; i < playerVector.size();i++)
		{
			saveFile << playerVector[i]->getPlayerID() << ",Infantry," << playerVector[i]->getCards()[0] << ",Cavalry," << playerVector[i]->getCards()[1] << ",Artillery," << playerVector[i]->getCards()[2];
			saveFile << ",Redemptions,";
			saveFile << playerVector[i]->getRedemptions() << "\n";
		}
		// finish save card

		saveFile << "Current Turn," << endl;

		saveFile << phase << "," << currentPlayer->getPlayerID();

		//save the turn

		saveFile.close();
	}//finish saving everything
	else 
		throw CorruptedSaveFile(CorruptedSaveFile::SAVE);

}

void Game::startPhaseForPlayer(Player* player,string phase){

	if (victoryCondition(player))
		return;

	int i= player->getPlayerID();

	if (phase=="reinforcement") 
		((i <= num_of_players-num_of_AI_players) ? (reinforcementPhase(dynamic_cast<HumanPlayer*>(playerVector[i-1]))) : (reinforcementPhase(dynamic_cast<ComputerPlayer*>(playerVector[i-1]))));
	else if (phase=="attack")
		((i <= num_of_players-num_of_AI_players) ? (attackPhase(dynamic_cast<HumanPlayer*>(playerVector[i-1]))) : (attackPhase(dynamic_cast<ComputerPlayer*>(playerVector[i-1]))));
	else if (phase=="fortification")
		((i <= num_of_players-num_of_AI_players) ? (fortificationPhase(dynamic_cast<HumanPlayer*>(playerVector[i-1]))) : (fortificationPhase(dynamic_cast<ComputerPlayer*>(playerVector[i-1]))));

	system("PAUSE");	

}


void Game::removePlayerFromGame(Player* player){

	if (player->getPlayerID() > (num_of_players -num_of_AI_players))
	{
		num_of_AI_players--;
	}

	num_of_players--;

	playerVector.erase (playerVector.begin()+ (player->getPlayerID()-1));	
}


void Game::handleMenuInput(string menuSelection,string phase, Player* player){

	if (menuSelection=="savegame")
	{
		saveGame(_map, view->queryUserForFileName(),phase, player);
		view->closeDisplay();
		playerViews[player->getPlayerID()]->closeDisplay();
		system("PAUSE");
		exit(0);
	}

	else if (menuSelection=="exit")
	{
		system("PAUSE");
		exit(0);
	}


}

Game::~Game() {
	_stats->closeDisplay();
}


