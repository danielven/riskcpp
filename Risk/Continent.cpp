//
//  Continent.cpp
//  Has an array of countries
//
#include "stdafx.h"
#include "Continent.h"

#include <string>


Continent::Continent(string name, int b)
{
	_name=name;
	bonus=b;

}

string Continent::getName(){
	return _name;
}

void Continent::printCountries(){

	for (vector<Country*>::iterator country_it = _countries.begin() ; country_it != _countries.end(); ++country_it){

		cout << (*country_it) -> getName() <<endl;
	}

}

bool Continent::countryExist(Country country){

	bool exist=false;

	for (vector<Country*>::iterator country_it = _countries.begin() ; country_it != _countries.end(); ++country_it)
	{
		if (country.equal((*country_it)))
		{
			exist=true;
			break;
		}
	}

	return exist;
}

bool Continent::addCountry(Country* country){

	if (find(_countries.begin(), _countries.end(),country)!=_countries.end())
		return false;
	else
	{
		(_countries.push_back(country));
		return true;
	}
}

bool Continent::equal(Continent* continent2){

	return _name==continent2->getName();
}

vector<Country*> Continent::getCountries(){
	return _countries;
}

int Continent::getBonus(){
	return bonus;
}

Continent::~Continent(){

}