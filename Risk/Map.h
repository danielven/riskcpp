//
//  Map.h
//

#ifndef MAP_H_
#define MAP_H_

# include <iostream>
# include <string>
# include <vector>
#include <fstream>
#include "Observable.h"

class Continent;
class Country;

using namespace std;

class Map : public Observable {
    
    public:
		Map();
        void addCountry(Country* countryA, Country* countryB);
		void addCountryToContinent(Country*,Continent*);
		void removeCountry(Country*);
		void addCountry(Country* country);
		void removeContinent(Continent*);
        void printMap();
		void Map::addContinent(Continent* continent);
		void Map::createMap(string);
		void Map::createMapWithArmies(string);
		Continent* getContinent(string continent);
		Country* getCountry(string country);
		void genericMapCreator();
		vector<Country*> getCountryVector();
		vector<Continent*> getContinentVector() {return _continents;} ;
		bool countryExist(Country* country);
		 bool Map::continentExist(Continent* continent);
    
    private:
        vector<Country*> _countries; //each country has an array of pointers to the countries it is adjacent to
		vector<Continent*> _continents;
         


};




#endif /* defined(____Map__) */
