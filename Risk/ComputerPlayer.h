#pragma once
#include "Player.h"
#include "Strategy.h"

class Strategy;


class ComputerPlayer:public Player
{

private:
	Strategy* strategy;
	static int num_of_AI_players;


public:
	ComputerPlayer() {};
	ComputerPlayer(string newName, int id) : Player (newName,id) {};
	
	ComputerPlayer::ComputerPlayer(string newName, int id, Strategy* strategy);

	static void setNumOfAIPlayers(int);
	static int getNumOfAIPlayers();
	Strategy* getStrategy() {return strategy;};
	void setStrategy(Strategy*);
	void executeStrategy();
	~ComputerPlayer();
};

