#include "stdafx.h"
#include "Observable.h"

Observable::Observable(){
	_observers = new list<Observer*>;
}
Observable::~Observable(){
	if (_observers->size()!=0)
		delete _observers;
}
void Observable::Attach(Observer* o){
	_observers->push_back(o);
}
void Observable::Detach(Observer* o){
	_observers->remove(o);
}

void Observable::Notify(){

	try{
		list<Observer *>::iterator i = _observers->begin();
	}
	catch(...)
	{
		return;
	}

	list<Observer *>::iterator i = _observers->begin();
	for (; i != _observers->end(); ++i)
		(*i)->Update();
}