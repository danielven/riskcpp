#include "stdafx.h"
#include "MapEditor.h"
#include <conio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <conio.h>
#include <algorithm>
#include <vector>
#include <sstream>


MapEditor::MapEditor(void)
{

	_map= new Map();

	_observer = new MapEditorView(this);



}

MapEditor::MapEditor(Map* map){

	_map=map;
	_observer = new MapEditorView(this);

}

MapEditor::~MapEditor(void)
{

	_observer->closeDisplay();
	delete _observer;
}

void MapEditor::loadMap(Map* map){

	_map=map;

	_observer = new MapEditorView(this);


}


Map* MapEditor::getMap(){

	return _map;
}

void MapEditor::beginConstruction(){

	_observer->display();

	int input = MapEditorView::options+1;

	while (input !=MapEditorView::options)
	{

		input = handleEvent();

	}
}

int MapEditor::handleEvent(){

	int input;

	input = _observer->promptUserForInput();

	if (input==1)
	{

		pair<Country*,Continent*> pair = _observer->buildCountry();

		_map->addCountryToContinent(pair.first,pair.second);

		_map->addCountry(pair.first);

	}

	return input;
}

//This is a method from an old assignment, not an ideal way to do this
void MapEditor::constructMapFile(){

	string input1,input2,input3,fileName,text,nameOfContinent,nameOfCountry,nameOfCC;
	int createOrLoad,continentOrCountry,addOrDelete,bonus,coordinate,nbOfArmy,coordinateX,coordinateY;
	vector<string> temp,before,connectedCountry,v;

	cout << "would you like to create a new map or load an existing map (enter 1 to create, enter 2 to load)" << '\n';
	cin >> createOrLoad;

	cout << '\n';

	if(createOrLoad == 1)
	{
		cout << "please enter the name of your new map as a .map file? (for exmaple: t.map)" << '\n';
		cin >> fileName;
		ofstream saveFile (fileName);
		if (saveFile.is_open())
		{
			saveFile << "Continents" << '\n';

			cout << '\n';
			cout << "Please enter the continent names or end to go next phase" << '\n';
			cin >> ws;
			getline(cin,input1);
			while(input1 != "end")
			{
				saveFile << input1 << ",";
				cout << "Please enter the bonus point of conquering this continent" << '\n';
				cin >> bonus;
				saveFile << bonus << '\n';
				cout << "Please enter the next continent names or end to go next phase" << '\n';
				cin >> ws;
				getline(cin,input1);
			}

			cout << '\n';

			saveFile << "Countries," << '\n';
			cout << "Please enter the country names or end to finish" << '\n';
			cin >> ws;
			getline(cin,input1);

			while(input1 != "end")
			{
				saveFile << input1 <<",";
				cout << "Please enter the x coordinate of the country" << '\n';
				cin >> coordinate;
				saveFile << coordinate <<",";
				cout << "Please enter the y coordinate of the country" << '\n';
				cin >> coordinate;
				saveFile << coordinate <<",";

				cout << "Please enter the continent of the country entered" << '\n';
				cin >> ws;
				getline(cin,input2);

				saveFile << input2;
				cout << "Please enter the country (or countries) that connected to the country entered, enter end to finish" << '\n';
				cin >> ws;
				getline(cin,input3);

				while(input3 != "end")
				{
					saveFile << "," << input3;

					cout << "Please enter the next connected country, enter end to finish" << '\n';
					cin >> ws;
					getline(cin,input3);
				}

				cout << "do you wish to add number of army to the country (enter yes or no)" << '\n';
				cin >> input3;
				if(input3 == "yes")
				{
					cout << "enter the number of army" << '\n';
					cin >> nbOfArmy;
					saveFile << "," << nbOfArmy;
				}
				saveFile << '\n';

				cout << '\n';
				cout << "Please enter the country names or end to finish" << '\n';
				cin >> ws;
				getline(cin,input1);

			}

			cout << "the new map is completed" << '\n';
			saveFile.close();
		}
		else cout << "Cannot create the map";

	}// end of creating new map

	else
	{
		cout << "Please the name of file that you wish to editing" << '\n' ;
		cin >> fileName;
		cout << '\n';

		cout << "would you like to modify the continent or the country? (enter 1 for continent, enter 2 for country)" << '\n' ;
		cin >> continentOrCountry;
		cout << '\n';

		if(continentOrCountry ==1)//modify the continent
		{
			cout << "would you like to add or delete a continent? (enter 1 for add, enter 2 for delete) " << '\n' ;
			cin >> addOrDelete;
			cout << '\n';

			if(addOrDelete == 1)//add continent
			{
				cout << "please enter the name of the continent that you would like to add " << '\n' ;
				cin >> ws;
				getline(cin,nameOfContinent);
				cout << "Please enter the bonus point of conquering this continent" << '\n';
				cin >> bonus;
				cout << " " << '\n' ;

				ifstream loadfile (fileName);
				if (loadfile.is_open())
				{
					while(getline(loadfile,text))
					{
						before.push_back(text);
						if(text == "Continents")
						{
							while(getline(loadfile,text))
							{
								temp.push_back(text);
								//cout << text << '\n';
							}
						}
					}


					loadfile.close();
				}
				else 
					cout << "Cannot load the existing map";

				ofstream saveFile (fileName);
				if (saveFile.is_open())
				{
					for(int t = 0; t < (int)before.size();t++)
					{
						saveFile << before[t] << '\n';
					}

					saveFile << nameOfContinent << ","<< bonus <<'\n';

					for(int t = 0; t < (int)temp.size();t++)
					{
						saveFile << temp[t] << '\n';
					}

					cout << nameOfContinent<< " was added to the continent list" << '\n';
					saveFile.close();
				}
				else cout << "Cannot save into the file";

			}

			else//delete continent
			{
				cout << "please enter the name of the continent that you would like to delete " << '\n' ;
				cin >> ws;
				getline(cin,nameOfContinent);
				cout << '\n';

				ifstream loadfile (fileName);
				if (loadfile.is_open())
				{
					while(getline(loadfile,text))
					{
						if( text != "")
						{
							string s = text;
							vector<string> v;
							istringstream b(s);
							for(string tok; getline(b, tok, ','); )
								v.push_back(tok);				

							if(v[0] != nameOfContinent)
							{
								temp.push_back(text);
							}
						}
					}

					loadfile.close();
				}
				else 
					cout << "Cannot load the existing map";

				ofstream saveFile (fileName);
				if (saveFile.is_open())
				{
					for(int t = 0; t < (int)temp.size();t++)
					{
						saveFile << temp[t] << '\n';
					}

					cout << nameOfContinent<< " was removed to the continent list" << '\n';
					saveFile.close();
				}
				else cout << "Cannot save into the file";
			}
		}//end of modifying the continent



		else
		{//modifying the country
			cout << "would you like to add or delete a country? (enter 1 for add, enter 2 for delete)" << '\n' ;
			cin >> addOrDelete;
			cout << '\n';

			if(addOrDelete == 1)//add country
			{
				cout << "please enter the name of the country that you would like to add " << '\n' ;
				cin >> ws;
				getline(cin,nameOfCountry);
				cout << '\n';

				cout << "Please enter the x coordinate of the country" << '\n';
				cin >> coordinateX;

				cout << "Please enter the y coordinate of the country" << '\n';
				cin >> coordinateY;


				cout << "please enter the name of the continent of " << nameOfCountry <<'\n' ;
				cin >> ws;
				getline(cin,nameOfContinent);
				cout << '\n';


				cout << "please enter the connected country of " << nameOfCountry <<'\n' ;
				cin >> ws;
				getline(cin,nameOfCC);

				while(nameOfCC != "end")
				{
					connectedCountry.push_back(nameOfCC);
					cout << '\n';

					cout << "please enter the next connected country or end to finish" << '\n' ;
					cin >> ws;
					getline(cin,nameOfCC);
				}
				cout << "do you wish to add number of army to the country (enter yes or no)" << '\n';
				cin >> input3;
				if(input3 == "yes")
				{
					cout << "enter the number of army" << '\n';
					cin >> nbOfArmy;
				}

				ifstream loadfile (fileName);
				if (loadfile.is_open())
				{
					while(getline(loadfile,text))
					{
						before.push_back(text);
						if(text == "Countries,")
						{
							while(getline(loadfile,text))
							{
								temp.push_back(text);
								//cout << text << '\n';
							}
						}
					}


					loadfile.close();
				}
				else 
					cout << "Cannot load the existing map";

				ofstream saveFile (fileName);
				if (saveFile.is_open())
				{
					for(int t = 0; t < (int) before.size();t++)
					{
						saveFile << before[t] << '\n';
					}

					saveFile << nameOfCountry << ',' << coordinateX <<","<< coordinateY << ",";
					saveFile << nameOfContinent;

					for(int t = 0; t < (int)connectedCountry.size();t++)
					{
						saveFile << ',' << connectedCountry[t];
					}

					if(nbOfArmy != -1)
					{
						saveFile << "," << nbOfArmy << '\n';
					}
					else
					{
						saveFile << '\n';
					}


					for(int t = 0; t < (int) temp.size();t++)
					{
						saveFile << temp[t] << '\n';
					}

					cout << nameOfCountry<< " was added to the country list" << '\n';
					saveFile.close();
				}
				else cout << "Cannot save into the file";
			}//end of adding a country

			else//delete country
			{
				cout << "please enter the name of the country that you wish to delete " << '\n' ;
				cin >> ws;
				getline(cin,nameOfCountry);
				cout << '\n';


				ifstream loadfile (fileName);
				if (loadfile.is_open())
				{
					while (getline(loadfile,text))
					{
						temp.push_back(text);
						if(text == "Countries,")
						{
							while (getline(loadfile,text))
							{
								//cout << text << '\n';
								if( text != "")
								{
									string s = text;
									vector<string> v;
									istringstream b(s);
									for(string tok; getline(b, tok, ','); )
										v.push_back(tok);				

									if(v[0] != nameOfCountry)
									{
										temp.push_back(text);
									}
								}
							}
						}
					}

					loadfile.close();
				}

				ofstream saveFile (fileName);
				if (saveFile.is_open())
				{
					for(int t = 0; t < (int)temp.size();t++)
					{
						saveFile << temp[t] << '\n';
					}

					cout << nameOfCountry<< " was removed to the country list" << '\n';
					saveFile.close();
				}
				else cout << "Cannot save into the file";

			}//end of delete country
		}
		cout << "you have finished editing the map, now move to the verification phase before saving" <<'\n';
	}//end of editing an existing map




	vector<string> continent,matchContinent,country,connectCountry;
	bool contcheck = true;
	bool contientConnectContient = false;
	continent.push_back("africa");
	continent.push_back("europe");
	continent.push_back("north america");
	continent.push_back("sourth america");

	ifstream loadfile (fileName);
	if (loadfile.is_open())
	{
		while (getline(loadfile,text))
		{
			if(text == "Countries,")
			{
				while (getline(loadfile,text))
				{
					//cout << text << '\n';
					if( text != ""){
						string s = text;
						vector<string> v;
						istringstream b(s);
						for(string tok; getline(b, tok, ','); )
							v.push_back(tok);

						//copy(v.begin(), v.end(),ostream_iterator<string>(cout," "));
						//cout << v[0] << '\n';
						country.push_back(v[0]);
						connectCountry.push_back(v[4]);
						matchContinent.push_back(v[3]);

						int countor = 0;
						for(int i = 0; i < (int)v.size(); i++){
							for(int m = 0; m <(int)continent.size();m++) // to check if every country only belongs to one and only one continent
							{
								if(v[i] == continent[m])
								{
									countor++;
									if(countor == 2)
									{
										cout << v[0] << " belongs more than 1 continent which is wrong" << '\n';
										contcheck = false;
									}
								}
							}

						}

					}

				}
			}

		}
		loadfile.close();
		cout << '\n';
		if(contcheck == true)
		{
			cout << "every country only belongs to one continent" << '\n';
		}

		int countor2 = 0;
		for(int j = 0; j < (int)country.size(); j++){ // check if every country connected to at least another one country
			for(int k = 0; k <(int)connectCountry.size();k++)
			{
				if(country[k] == connectCountry[j])
				{
					countor2++;
					if(matchContinent[k] != matchContinent[j])// if one country's continent is not the same as the one country connected, it means the whole graph is connect.
					{
						contientConnectContient = true;
					}
				}
			}
		}
		if(countor2 == country.size())// if the number of count matches the number of country, it means this is a connect graph
		{
			cout << "every country is connected to another country, therefore it's a sub connect graph" << '\n';
		}
		if(contientConnectContient)
		{
			cout << "every contient is connected to another contient, therefore it's connected graph" << '\n';
			cout << "verification completed" << '\n';
		}

	}

	else {
		cout << "There is no such file"; 
	}


	cout << "program terminated" << '\n';

}