
#ifndef Game_H_
#define Game_H_
#include <array>
#include <list>
#include "Map.h"
#include "Player.h"
#include "ComputerPlayer.h"
#include "HumanPlayer.h"
#include "Observable.h"
#include "PlayerView.h"
#include "GameView.h"
#include "AggressiveStrat.h"
#include "DefensiveStrat.h"
#include "RandomStrat.h"
#include "BasicStats.h"

using namespace std;

class GameView;

class Game : public Observable{

	friend class GameBuilder;
	

private:

	int static const max_players=6;
	vector<Player*> playerVector;
	int num_of_players;
	int num_of_AI_players;
	int max_num_countries;
	void reinforcementPhase(HumanPlayer*);
	void attackPhase(HumanPlayer*);			
	void attackPhase(ComputerPlayer*);
	void fortificationPhase(HumanPlayer*);	
	void fortificationPhase(ComputerPlayer*);
	void randomlyShuffle(Country *array, int size);
	bool victoryCondition();
	int redeemCards(Player*);
	int redeemCards(ComputerPlayer*);
	bool victoryCondition(Player * player);
	void randomlyShuffle(vector<Country*> *vector);
	Player* winningPlayer();
	GameView* view;
	vector<PlayerView*> playerViews;
	Map* _map;
	void reinforcePlayer(int,Player*);
	BasicStats* _stats;
	void reinforcePlayer(int,HumanPlayer*);
	void reinforcePlayer(int,ComputerPlayer*);
	void startPhaseForPlayer(Player*,string);
	void removePlayerFromGame(Player*);

	int calculateReinforcingArmiesForPlayer(Player*);

public:
		Game();
		Game(Map *map);
		vector<Player*> getPlayerVector();
		void initializePlayers();
		void randomCountryAssign(Country *array, int size);		
		list<ComputerPlayer*> getComputerPlayerList();
		int getNumOfPlayers();
		void randomCountryAssign(Map*);
		void randomlyAssignArmies();
		void addPlayer(HumanPlayer*);
		void addPlayer(ComputerPlayer*);
		void printGameState();
		void startGame();
		vector<PlayerView*> getPlayerViewVector();
		void handleMenuInput(string, string,Player*);
		void reinforcementPhase(ComputerPlayer*);
		void saveGame(Map* map,string fileName,string phase,Player* currentPlayerPhase);
		virtual ~Game();

protected:
		void startGame(int, string); //Methods only used by the builder class. Inputs are id and string
		void setMap(Map* map);
		
};

#endif /* Game_H_ */
