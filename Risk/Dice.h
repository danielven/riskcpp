#pragma once
#include <list>
using namespace std;

class Dice
{
private:
	static int roll();
public:
	Dice(void);
	~Dice(void);
	// Fill the lists with rolled dice
	static void rollDiceForPlayers(list<int>* atk, list<int>* def, int numOfAttackingArmies, int numOfDefendingArmies);
};

