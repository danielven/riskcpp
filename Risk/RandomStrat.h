#pragma once
#include "strategy.h"
#include <map>
#include <set>

class RandomStrat : public Strategy
{
public:
	const static string className;
	RandomStrat(void);
	virtual string getStrategyName() {return className;};
		virtual void execute(ComputerPlayer*);
	~RandomStrat(void);
};

