#pragma once
#include "MapEditor.h"
#include "Observer.h"
#include "ConsoleUI.h"
#include "Continent.h"

class MapEditor;

class MapEditorView : public Observer, public ConsoleUI
{


public:
	MapEditorView() {};
	MapEditorView(MapEditor*);
	void Update();
	int promptUserForInput();
	pair<Country*,Continent*> buildCountry();
	~MapEditorView(void);
	const static int options=5;

private:
	void displaySubjectData();
	void handleSelection(int);
	Map* _map;
	MapEditor* _subject;
};

