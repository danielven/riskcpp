#include "stdafx.h"
#include "RandomStrat.h"
#include <time.h>       
#include <cstdlib>


using namespace std;

const string RandomStrat::className="Random";

RandomStrat::RandomStrat()
{
}


//Attacks a random enemy player. Attacks may happen more than once. Probably not the best way to induce randomness, but the assignment doesn't enphasize the best implementation of the interior of this method. Changing the data types to vectors to enable random access iterators is an idea for going forward.
void RandomStrat::execute(ComputerPlayer* player)
{
	int atck=rand() % 2;

	//First determine whether an attack will happen.

	if (atck==0)
	{
		cout << "Random player " << player->getPlayerID() << " (" << player->getName() << ") abstains." << endl;
		return;
	}



	while (atck==1)
	{
		map<Country*,set<Country*>> playerMap=player->getCountryMap();

		int mapSize=playerMap.size();

		int index1=0;

		for (map<Country*,set<Country*>>::iterator it= playerMap.begin(); it!=playerMap.end(); it++)				
		{
			//If the last country is reached and no attack happens, the last country attacks
			if (rand() % 2 || mapSize==index1)
			{
				int setSize=it->second.size();

				int index2=0;

				for(set<Country*>::iterator it2= it->second.begin(); it2!=it->second.end(); it2++)
				{

					//If the last country is reached and no attack happens, the last country attacks
					if (rand() % 2 || setSize==index2)

					{
						Battle battle(it->first, *it2);

						battle.doBattle(false);

						break;

					}
					index2++;

				}

				break;
			}

			index1++;

		}


		atck=rand() % 2;
	}

	cout << "Random player " << player->getPlayerID() << " (" << player->getName() << ") stops attacking." << endl<<endl;


}



RandomStrat::~RandomStrat(void)
{
}
