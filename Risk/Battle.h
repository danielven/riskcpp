#pragma once
#include "Country.h"
#include "BattleView.h"
#include <list>
class Battle
{
private:
	Country *attacker, *defender;
	BattleView* view;
	bool status;
	void transferCards(Country*, Country*);

	// method that handles the conquering of a defending Country by the attacker. Takes as input the number of armies the attacker last attacked with.
	void conquer(int);

	// check if battle is done
	void setStatus(bool done);
	bool getStatus();
	// all in attack: attacker continues to attack until either it runs out of armies, or the defender runs out of armies
	void allIn();
public:
	Battle(Country* const &atk, Country* const &def);
	// Default Constructor
	Battle();
	
	bool doBattle(bool); // This method simulates a battle. Must input a boolean argument to determine whether the attacking player is all in or not (true for all in). Returns true if a conquer happened.
	virtual ~Battle();
};