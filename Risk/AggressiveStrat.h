#pragma once
#include "strategy.h"
#include <map>
#include <set>
class AggressiveStrat :
	public Strategy
{
public:
	static const string className;
	AggressiveStrat();
	virtual string getStrategyName() {return className;};
	virtual void execute(ComputerPlayer*);
	~AggressiveStrat();
};

