Risk Final Delivery
Comp345
by
Jesus Imery
Daniel Ventulieri
Justin Jiabing Ying
April 14th, 2015
Deliverable

We are submitting a Visual C++ project that emulates a Risk computer game.  Codebase contains all of the classes necessary for compiling and running the game. The driver initiates the game through method calls. To compile, preferably build the solution through Visual Studio. The project contains relative link paths to the library, so they are already setup so long as you do not move the location of the library files.
The main focus during the design of the game was follow MVC design. The classes are separated into models, controllers and viewers that interact with the user. 
The driver provided to test the game allows for creating a game from scratch, loading a game from a .txt file or testing the map editor. Several test game .txt files are provided with the project folder.
Overall Description of the Code Base

The current version of our game, following Risk rules, supports up to 6 players, with at least two players. Once a game begins it will continue until a player conquers all countries or the user tells the game to stop between game phases. Strategy patterns have been set for the computer players, which can employ a defensive or aggressive strategy. Some of the phases don�t yet have proper handling for the AI players so the game will prompt the user for inputs with them. The ComputerPlayer class inherits from the Player class and DefensiveStrat / RandomStrat / AggressiveStrat inherit from the Strategy class. We also have a HumanPlayer that inherits from the Player class. 
The majority of the governing game logic is found in the Game controller class, which contains most of the game features.  All of the battle logic is found in the Battle controller class. It handles individual and all-in attacks, as well as deciding the victor and conquering. To start a Battle, we initialize a Battle object with the attacking country and defending country as parameters.
The Map class is capable of generating the generic map to be used in Risk, by reading a text file based on the ones provided by http://www.windowsgames.co.uk. This can also be achieved more extensively by the GameBuilder, which creates a Game and a Map by reading a map file (usually text based). 
At the start of each game, we give the user the option to create a new game, to load a game, or to launch the map editor. When selecting new game, the program will run the user through the process of starting a new game from scratch. At any point during the game, the user may type the save command, which will save the game to a file. If the user selects load (at launch), the saved game can be selected and the user will start from where they left off. The map editor allows users to create a brand new map from scratch or to edit saved maps.
The loading of the game is handled by a builder class for constructing a game. The steps for this are demonstrated with the driver. This builder pattern design does not use a director, since there is only one type of game (one concrete subject) to build. A director could be necessary, for example, if we were creating different types of Risk game (Risk Legacy, Risk Halo Wars etc).
We have decided to opt for a text based interface. When the program starts, the interactive console guides the user through the game. This is called the Game View. We also have a separate console window for the game stats and menu. This window shows the armies, cards, and countries owned by each player as well as the save and exit commands. As the game progresses, PlayerView windows are opened whenever a player is involved. For example, if it�s Player 2�s turn, a PlayerView window containing Player 2�s information will open. To close the window, simply press Enter in the window when the player�s turn is over. 
All this view classes have been implemented through observer patterns, and you will notice during gameplay that they are notified and properly updated by their observed subjects.
We worked really hard to implement an interactive MapView that tracks the game information and displays the owner of each country as well as the armies of each country in the map. The players were going to be color coded so we could see each player�s progression around the map. This was the last feature to be added, but the library used, SFML, needed to be executed through a while loop containing the entire game driver. This conflicted with are intended design to that point, since the controllers in our program instantiate viewer classes when necessary, and start or stop displays. To implement SFML, the viewer would have to contain the controller within its loop, effectively granting it more hierarchy. We believe this could be solved with multithreading, but looked no further into it since the focus is not in graphical interfacing. We left the MapView file as well as the SFML libs with the files to show our work, though they are not included in the solution. 

Important Design Decisions Specific to this Assignment
As a team, we really tried to organize our code in an MVC fashion. Our model classes Player, Country, and Continent have properties and getters and setters. They have logic to control themselves, but we kept that logic to a minimum. Controller classes like Battle or Game can manipulate these model classes. For example, the Battle class has a conquer method which simulates one country conquering another. We also have view classes which are aware of the model classes, but contain minimal logic. For example, the BasicStats view observes Players and Countries and displays informative statistics to the user at all times. We also have a PlayerView class that observes players and displays their information, as well as queries the user for input. 
Our views inherit from the ConsoleUI class. It is an interface with all the necessary methods to correctly implement and use our implementation for game display. Whenever the developer wants to display a view, we call PlayerView.display() (for example), which creates a new ConsoleUI window. This allows us to keep these views separate from the console application which can take input from the user. 
Separation of concerns was also an objective while coding. A perfect example of this is our Battle module. The Battle class handles all of the logic of a battle, without any couts. We abstracted all of the user interactions away, so the class can focus only on the logic. When it needs to query the user or display information to the user, it calls methods from the BattleView class, which can send output or query the user for input. It then returns the input in the specified form (i.e. int, string, etc.) to the Battle. A benefit of this is that it would be fairly trivial to swap out the text base interface for a GUI interface. The Battle would not need any changes, at its user interactions are abstracted away by the BattleView. Another example would be the Dice class. It has one job: to simulate a dice roll. 


Important Rules for the Project

The following are the major rules relevant to the project delivered to this point:
�	Minimum of two players, and a maximum of 6.
�	Players can only attack countries that are adjacent to a country they own.
�	Players can attack with at most 3 armies.
�	Players can defend with at most 2 armies.
�	During a battle, each player rolls dice equal to the number of armies they are involving in said battle. Whichever player rolls a higher number wins, and an army is subtracted from the loser.
�	When attacking, at least one army must be left back in the attacking country. If a country has only 1 army, it can�t attack. This is why a reinforce phase is a necessity.
�	If the defending country loses all its armies during an attack, the attacking player must conquer the defending country, moving at least as many armies as he/she attacked with, with the option of moving as many as he/she wants leaving at least one back in the attacking country.
�	A random card is given to the player when conquering a country.
�	All cards are taken by the conquering player from the conquered one when a battle is lost.

Engineering Tools

Throughout the development of this project, we�ve used several engineering tools. Before even beginning to code the program, we set up a private Git repository on BitBucket (bitbucket.org). We decided to go with BitBucket rather than GitHub because they offer free private repositories. Source control was very important for us in this project. It allowed us to easily merge our code and stay up to date. We also utilized a .gitignore file to keep compiled code out of git repository. 
Another tool we used is Microsoft Visual Studio. We quickly realized that using Makefile to compile would create way too much overhead. Visual Studio facilitated the compilation and debugging of our program. It also allowed us to easily add third party libraries when needed.
