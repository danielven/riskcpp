#include "stdafx.h"
#include "ComputerPlayer.h"

int ComputerPlayer::num_of_AI_players=0;



ComputerPlayer::ComputerPlayer(string newName,int id, Strategy* strategy){

	playerID= id;
	countriesOwned = 0;
	name=newName;
	for (int i=0; i<numberOfCardTypes; i++)
		ownedCards[i]=0;
	numberOfRedemptions=0;

	setStrategy(strategy);
}

void ComputerPlayer::setStrategy(Strategy* strat)
{
	strategy=strat;

}

void ComputerPlayer::executeStrategy()
{
	strategy->execute(this);
}


void ComputerPlayer::setNumOfAIPlayers(int num)
{
	num_of_AI_players=num;

}

int ComputerPlayer::getNumOfAIPlayers()
{
	return num_of_AI_players;

}

ComputerPlayer::~ComputerPlayer(void)
{
}
