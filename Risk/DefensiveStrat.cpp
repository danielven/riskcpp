#include "stdafx.h"
#include "DefensiveStrat.h"

const string DefensiveStrat::className="Defensive";

DefensiveStrat::DefensiveStrat(void)
{
}

void DefensiveStrat::execute(ComputerPlayer* player){

	set<Country*> strongDefensiveCountrySet=player->getStrongDefensiveCountrySet();

	if (strongDefensiveCountrySet.empty())
	{
		cout << "Defensive player " << player->getPlayerID() << " (" << player->getName() << ") abstains." << endl;
		return;
	}


	//Iterator it is strong countries owned by potential attacking player
	for (set<Country*>::iterator it=strongDefensiveCountrySet.begin(); it!=strongDefensiveCountrySet.end(); it++)
	{
		vector<Country*> adj=(*it)->getAdjacentCountries();

		//Iterator it2 is countries adjacent to it. This should be fixed in the future, its attacking one of the adjacent countries according to its position in the vector, should probably be random. Vector shuffle is an idea for the future.
		for(vector<Country*>::iterator it2=adj.begin(); it2!=adj.end(); it2++)
		{
			//Rechecks the condition (that all enemy adjacent countries have less armies) after every battle.
			while((*it)->getPlayer()->checkForAllWeakAdjacent(*it) && (*it2)->getPlayer()!=(*it)->getPlayer())
			{
				Battle battle(*it, *it2);

				battle.doBattle(false);

			}

		}

	}

	cout << "Defensive player " << player->getPlayerID() << " (" << player->getName() << ") stops attacking." << endl<<endl;

}


DefensiveStrat::~DefensiveStrat(void)
{
}
