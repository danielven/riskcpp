#pragma once
#include "Map.h"
#include "MapEditorView.h"
#include "Observable.h"

class MapEditorView;

class MapEditor : public Observable
{
private:
	Map* _map;
	MapEditorView* _observer;
	int handleEvent();
public:
	MapEditor();
	MapEditor(Map* map);
	void beginConstruction();
	void loadMap(Map* map);
	
	Map* getMap();
	void constructMapFile();
	~MapEditor(void);
};

