#include "stdafx.h"
#include "ContinentAlreadyExists.h"


ContinentAlreadyExists::ContinentAlreadyExists(void)
{
}

const char* ContinentAlreadyExists::what() {
	return "Continent is already in the Map!";
}

ContinentAlreadyExists::~ContinentAlreadyExists(void)
{
}
