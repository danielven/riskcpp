/*
	Battle View handles the couts of a Battle. Each Battle creates a new BattleView object and calls its methods.
*/

#pragma once
#include "Country.h"
#include "Player.h"
#include "ConsoleUI.h"

class BattleView : public ConsoleUI
{

private:
	void displaySubjectData() {};  //No implementation unless the user wishes for additional consoles

public:
	BattleView(void);

	void notEnoughArmies(Country* attacker, Country* defender);
	void displayAttackerInfo(Country* attacker);
	void displayDefenderInfo(Country* defender);
	int getNumberOfAttackingArmies(Country* attacker, const int maxNumberOfArmies);
	void battleStarting(Country* attacker, Country* defender, int numOfAttackingArmies, int numOfDefendingArmies);
	void displayStalemate(int rolled);
	void attackerRolledHigher(Country* defender, int attackerRoll, int defenderRoll);
	void defenderRolledHigher(Country* attacker, int attackerRoll, int defenderRoll);
	void defenderWonBattle();
	void attackerWonBattle();
	bool continueAttackingQuery(Country* attacker, Country* defender);
	int conquerQuery(Country* attacker, Country* defender, int numOfAttackingArmies);

	~BattleView(void);
};

