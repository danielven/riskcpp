#pragma once
#include "strategy.h"
#include <map>
#include <set>
class DefensiveStrat:
		public Strategy

{

public:
	DefensiveStrat(void);
	const static string className;
	virtual string getStrategyName() {return className;};
		virtual void execute(ComputerPlayer*);
	~DefensiveStrat(void);
};

