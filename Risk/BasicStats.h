#pragma once
#include "ConsoleUI.h"
#include "Player.h"
#include "Observer.h"

using namespace std;

class BasicStats : public Observer, public ConsoleUI
{
public:
	// We can pass a Game directly to the statistic object to observe all the players in the game
	BasicStats(vector<Player*> players);
	BasicStats();
	~BasicStats();	
	void observe(Player* player);
	// Again, we can pass a Game directly to the statistic object to observe all the players in the game
	void observe(vector<Player*> players);
	void Update();
private:
	// All three maps map a player ID (int) to another int, either the # of countries controlled, the # of armies per player, the # of cards per player
	list<Player*> _subjects;
	string getPlayersAndArmies();
	void getStats();
	void displaySubjectData();
	void displayMenuOptions();
};

