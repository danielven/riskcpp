#pragma once
#include "Observer.h"
#include "Player.h"
#include "ConsoleUI.h"
class PlayerView : public Observer, public ConsoleUI
{
public:
	PlayerView(Player*);
	void Update();
	void displayNumberOfReinforcingArmies();
	static void printCountriesAndArmies(Player*); 
	static void printCountriesAndArmies(vector<Country*>);
	static void printFullCountryDetails(vector<Country*>);

	void attachCountryToPlayerView(Country*);
	void detatchCountryFromPlayerView(Country*);

	int redeemCards();
	void printCards();
	
	~PlayerView(void);
private:
	
	Player* _playerObserved;
	void displaySubjectData();
	void displayCards();
	void displayCountries(const vector<Country*>);
	void displayContinentConquered(const set<Continent*>);
};

