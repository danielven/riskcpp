/*
 * Player.h
 *	Players own countries when he/she places armies on them, or conquers. These functionalities have yet to by added. For this example,
 *	dummy countries are added to the game and then one player will be forced countries until the game ends.
 * 
 */
#ifndef PLAYER_H_
#define PLAYER_H_

#include "Country.h"
#include "Continent.h"
#include "Observable.h"
#include <vector>
#include <string>
#include <set>
#include <map>


using namespace std;

class Strategy;

class Country;
 
class Player : public Observable {

	friend class Game;

	public:
	static enum Card {INFANTRY=0,CAVALRY=1,ARTILLERY=2};
	const static int numberOfCardTypes=3;   //This variable is here to allow ease of modification in the future.

	protected:
	string name;
	static const string* cardNames;
	int playerID;
	int countriesOwned;
	int numberOfRedemptions;
	int ownedCards[numberOfCardTypes];
	vector <Country*> countryVector;	
	set<Continent*> continentSet; //Set of continents that have been completely conquered by the player.
	
	set<Country*> getStrongCountries();        
	set<Country*> getWeakEnemyCountries();

	 static Card makeRandomCard();
	 
	 	 
	 void updateContinentSet(Continent*);			
	 
	

public:
	Player();
	Player(string, int);

	int getCountriesOwned();
	int getPlayerID();
	int getNumberOfArmies();
	int getTotalNumberOfCards();
	int getRedemptions();
	int * getCards();
	static string getCardName(int);
	string getName();
	set<Continent*> getContinentSet();
	map<Country*, set<Country*>> getCountryMap();
	map<Country*, set<Country*>> getStrongCountryMap();

	 void addCountry(Country* const &addedCountry);
	 
	 vector<Country*> getCountryVector();
	 
	 set<Country*> getAdjacentEnemyCountries(); //Sets are chosen because we want elements to be unique.
	 set<Country*> getStrongDefensiveCountrySet();
	
	 bool checkForAllWeakAdjacent(Country*);

	 void substractCountry(Country*);
	 void addCard();
	 void addCard(int);
	 void substractCard(int);
	 void addRedemption();
	

	
	virtual ~Player();
};

#endif /* PLAYER_H_ */
