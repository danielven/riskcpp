#include "stdafx.h"
#include "PlayerView.h"
#include <sstream>

using namespace std;

PlayerView::PlayerView(Player * player)
{
	_playerObserved=player;

	player->Attach(this);
}

void PlayerView::attachCountryToPlayerView(Country* country){

	country->Attach(this);
}

void PlayerView::detatchCountryFromPlayerView(Country* country){

	country->Detach(this);
}


void PlayerView::displayContinentConquered(set<Continent*> continentSet){

	_console->printf("\nCONTINENTS\n");
	displaySeparatorLine();

	if (continentSet.empty())
	{
		_console->printf("No continents conquered.\n");
		return;
	}

	_console->printf("%d",continentSet.size());
	_console->printf(" Continents Conquered.");

	for (set<Continent*>::iterator it = continentSet.begin(); it != continentSet.end(); ++it) {
		_console->printf("\n* ");
		_console->printf(((*it)->getName()).c_str());
		int nameSize = ((*it)->getName().size());
		for (nameSize; nameSize<ConsoleUI::secondColumnDisplayPosition; nameSize++)
			_console->printf(" ");

		_console->printf("%d",(*it)->getBonus());

		_console->printf(" bonus armies.\n");
	}


}




void PlayerView::displayCountries(vector<Country*> countryVector){

	_console->printf("\nCONTRIES\n");
	displaySeparatorLine();

	if (countryVector.empty())
	{
		_console->printf("No countries owned.\n");
		return;
	}

	_console->printf("%d",countryVector.size());
	_console->printf(" Countries, ");
	_console->printf("%d",_playerObserved->getNumberOfArmies());
	_console->printf(" Armies.\n");

	for (vector<Country*>::iterator it = countryVector.begin(); it != countryVector.end(); ++it) {
		{
			_console->printf("* ");
			_console->printf(((*it)->getName()).c_str());
			int nameSize = ((*it)->getName().size());
			for (nameSize; nameSize<ConsoleUI::secondColumnDisplayPosition; nameSize++)
				_console->printf(" ");

			_console->printf("%d",(*it)->getArmies());

			if ((*it)->getArmies()==1)
				_console->printf(" army.\n");
			else
				_console->printf(" armies.\n");

		}

	}
}

void PlayerView::printCountriesAndArmies(Player* player){

	vector<Country*> countryVector = player->getCountryVector();

	if (countryVector.empty())
	{
		cout << ("No countries owned.\n");
		return;
	}

	int i=0;

	printSeparatorLine();

	for (vector<Country*>::iterator it = countryVector.begin(); it != countryVector.end(); ++it) {
		{

			cout << ++i << ":";
			cout << (((*it)->getName()).c_str());
			int nameSize=(*it)->getName().size();
			for (nameSize; nameSize<ConsoleUI::secondColumnDisplayPosition; nameSize++)
				cout << (" ");
			cout << (*it)->getArmies() << (((*it)->getArmies() > 1 || (*it)->getArmies() == 0) ? " armies." : " army.") << endl;

		}

	}

}

void PlayerView::printCountriesAndArmies(vector<Country*> countryVector){

	if (countryVector.empty())
	{
		cout << ("No available countries.\n");
		return;
	}

	int i=0;

	printSeparatorLine();

	for (vector<Country*>::iterator it = countryVector.begin(); it != countryVector.end(); ++it) {
		{

			cout << ++i << ":";
			cout << (((*it)->getName()).c_str());
			int nameSize=(*it)->getName().size();
			for (nameSize; nameSize<ConsoleUI::secondColumnDisplayPosition; nameSize++)
				cout << (" ");
			cout << (*it)->getArmies() << (((*it)->getArmies() > 1 || (*it)->getArmies() == 0) ? " armies." : " army.") << endl;

		}

	}

}

//Includes Players that own the countries
void PlayerView::printFullCountryDetails(vector<Country*> countryVector){

	if (countryVector.empty())
	{
		cout << ("No available countries.\n");
		return;
	}

	int i=0;

	printLongSeparatorLine();

	for (vector<Country*>::iterator it = countryVector.begin(); it != countryVector.end(); ++it) {
		{

			cout << ++i << ":";
			cout << (((*it)->getName()).c_str());
			int nameSize=(*it)->getName().size();
			for (nameSize; nameSize<ConsoleUI::secondColumnDisplayPosition; nameSize++)
				cout << (" ");
			stringstream armyDisplay;
			armyDisplay << (*it)->getArmies() << (((*it)->getArmies() > 1 || (*it)->getArmies() == 0) ? " armies" : " army");
			cout << armyDisplay.str();
			armyDisplay.seekg(0, ios::end);
			nameSize=0;
			nameSize += armyDisplay.tellg();
			for (nameSize; nameSize<ConsoleUI::secondColumnDisplayPosition-10; nameSize++)
				cout << " ";
			cout << "Player " << (*it)->getPlayer()->getPlayerID() << " (" << (*it)->getPlayer()->getName() << ")." << endl;

		}

	}

}



void PlayerView::displayCards(){

	_console->printf("\nCARDS\n");
	displaySeparatorLine();

	bool hit=false;

	for (int i=0; i<Player::numberOfCardTypes; i++)
	{	
		if (_playerObserved->getCards()[i]>0)
		{
			hit=true;
			_console->printf("%d",_playerObserved->getCards()[i]);
			_console->printf(" ");
			_console->printf(Player::getCardName(i).c_str());
			_console->printf(" cards.\n");
		}
	}

	if (hit==false)
		_console->printf("No cards owned\n");

	_console->printf("%d",_playerObserved->getRedemptions());
	_console->printf((_playerObserved->getRedemptions()==1 ? " redemption.\n" : " redemptions.\n"));

}

void PlayerView::printCards(){

	bool hit=false;

	cout << "Available cards: " << endl;

	for (int i=0; i<Player::numberOfCardTypes; i++)
	{	
		if (_playerObserved->getCards()[i]>0)
		{
			hit=true;
			cout<< _playerObserved->getCards()[i];
			cout << printf(" ");
			cout << (Player::getCardName(i).c_str());
			cout << printf(" cards.\n");
		}
	}

	if (hit==false)
		_console->printf("No cards owned\n");
}



void PlayerView::Update(){

	_console->cls();

	displaySubjectData();
}

void PlayerView::displaySubjectData(){

	_console->printf(("Player " + to_string(_playerObserved->getPlayerID()) + ": " + _playerObserved->getName() + ".\n").c_str());

	vector<Country*> countryVector = _playerObserved->getCountryVector();

	set<Continent*> continentSet = _playerObserved->getContinentSet();

	displayCountries(countryVector);

	displayCards();

	displayContinentConquered(continentSet);
}

PlayerView::~PlayerView(void)
{
	_playerObserved->Detach(this);
}