#include "stdafx.h"
#include "Dice.h"
#include <cstdlib>
#include <ctime>


void Dice::rollDiceForPlayers(list<int>* atk, list<int>* def, int numOfAttackingArmies, int numOfDefendingArmies) {
	// fill the lists
	for (int i = 0; i < numOfAttackingArmies; i++) {
		atk->push_back(roll());
	}
	for (int i = 0; i < numOfDefendingArmies; ++i) {
		def->push_back(roll());
	}

	// Sort the lists
	atk->sort();
	def->sort();
}

int Dice::roll() {
	// returns random number between 1-6 inclusively
	return rand() % 6 + 1;
}

Dice::Dice(void)
{
}


Dice::~Dice(void)
{
}
